package com.gennext.lpz;

import android.os.Bundle;

import com.gennext.lpz.shop.DailyNeeds;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar=setToolBar("MLM");
//        SetDrawer(MainActivity.this, toolbar);

        initUi();
    }

    private void initUi() {
        addFragment(new DailyNeeds(),"dailyNeeds");
    }
}
