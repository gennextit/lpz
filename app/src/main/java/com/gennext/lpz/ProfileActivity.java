package com.gennext.lpz;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gennext.lpz.profile.Profile;
import com.gennext.lpz.util.AppTokens;


/**
 * Created by Abhijit on 14-Oct-16.
 */

public class ProfileActivity extends BaseActivity {

    FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = getSupportFragmentManager();
        startScreen();
    }

    private void startScreen() {
        Profile profile = new Profile();
        profile.setOpenStatus(Profile.CHECKOUT_SCREEN);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, profile, "profile");
        transaction.commit();
    }


    public void onSuccessUpdatedProfile() {
        AppTokens.setSessionProfile(getApplicationContext(), "success");
        finish();
    }
}
