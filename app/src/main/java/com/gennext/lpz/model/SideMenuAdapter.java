package com.gennext.lpz.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.gennext.lpz.R;

import java.util.List;


public class SideMenuAdapter extends ArrayAdapter<SideMenu>{
	List<SideMenu> sideMenuList;
	private Activity activity;
    private LayoutInflater inflater;
    int resource;
    
	public SideMenuAdapter(Activity activity, int resource, List<SideMenu> sideMenuList) {
    	super(activity, resource, sideMenuList);
    	this.activity = activity;
    	this.resource = resource;
    	this.sideMenuList = sideMenuList;		
	}

	public class ViewHolder {
    	TextView name;
		ImageView image;
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		  if (inflater == null)
	            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(resource, parent, false);
			holder.name = (TextView) view.findViewById(R.id.name);
			holder.image = (ImageView) view.findViewById(R.id.image);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		
		
		holder.name.setText(sideMenuList.get(position).getName());
		holder.image.setImageResource(sideMenuList.get(position).getImage());
		
		return view;
	}
}
