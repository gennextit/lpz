package com.gennext.lpz.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.gennext.lpz.R;
import com.gennext.lpz.util.DBManager;

import java.util.ArrayList;

/**
 * Created by Abhijit on 11-Oct-16.
 */
public class OrderPopupAdapter extends ArrayAdapter<OrderHistoryModel> {
    private final ArrayList<OrderHistoryModel> list;

//    ImageLoader imageLoader;
    DBManager db;
    private final Activity context;
    private String calPrice;

    public OrderPopupAdapter(Activity context, int textViewResourceId, ArrayList<OrderHistoryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        db = new DBManager(context);
//        imageLoader=new ImageLoader(context);
    }

    class ViewHolder {
        TextView tvProductName, tvQuantity, tvPrice;
        ImageView ivImage;

        public ViewHolder(View v) {
            ivImage = (ImageView) v.findViewById(R.id.iv_slot_order_popup);
            tvProductName = (TextView) v.findViewById(R.id.tv_slot_order_popup_name);
            tvQuantity = (TextView) v.findViewById(R.id.tv_slot_order_popup_quantity);
            tvPrice = (TextView) v.findViewById(R.id.tv_slot_order_popup_price);

        }
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_order_popup, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        String name=list.get(position).getProductName();
        if(name!=null){
            holder.tvProductName.setText(name);
        }
        String price=list.get(position).getDetailPrice();
        if(price!=null){
            holder.tvPrice.setText(price);
        }
        String quantity=list.get(position).getDetailQuantity();
        if(quantity!=null){
            holder.tvQuantity.setText("Quantity "+quantity);
        }
//        String image= Utility.encodeUrl(list.get(position).getImageUrl());
        String image=list.get(position).getImageUrl();

        if(image!=null){
            Glide.with(context)
                    .load(image)
                    .placeholder(R.drawable.ic_error_loading_image)
                    .error(R.drawable.ic_error_loading_image)
                    .into(holder.ivImage);

//            imageLoader.DisplayImage(image,holder.ivImage,"blank",false);
        }
        return v;
    }


}