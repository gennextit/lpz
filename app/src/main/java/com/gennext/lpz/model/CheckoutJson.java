package com.gennext.lpz.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Oct-16.
 */


public class CheckoutJson {

    public static String toJSonArray(ArrayList<DailyNeedsModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            if (list != null) {
                for (DailyNeedsModel model : list) {
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("categoryId", model.getCategoryId());
                    pnObj.put("subCategoryId", model.getSubCategoryId());
                    pnObj.put("productId", model.getProductID());
                    pnObj.put("productName", model.getProductName());
                    pnObj.put("productPrice", model.getProductRetailPrice());
                    pnObj.put("productQuantity", model.getQuantity());
                    pnObj.put("imageUrl", model.getProductImage());
                    jsonArr.put(pnObj);
                    //jsonOBJ.put(pnObj);
                }
            } else {
                return null;
            }
            return jsonArr.toString();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;

    }


}