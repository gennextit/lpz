package com.gennext.lpz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 18-Oct-16.
 */

public class OrderHistoryModel {

    private String orderId;
    private String orderTrackId;
    private String userId;
    private String orderTotalAmount;
    private String paymentMethod;
    private String orderShipName;
    private String orderShipAddress;
    private String orderPhone;
    private String orderDate;
    private String orderTime;
    private String details;
    private String status;

    // product detail
    private int prodPosition;
    private String productName;
    private String detailPrice;
    private String imageUrl;
    private String detailQuantity;



    private String output;
    private String outputMsg;

    public int getProdPosition() {
        return prodPosition;
    }

    public void setProdPosition(int prodPosition) {
        this.prodPosition = prodPosition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private ArrayList<OrderHistoryModel> list;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDetailPrice() {
        return detailPrice;
    }

    public void setDetailPrice(String detailPrice) {
        this.detailPrice = detailPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDetailQuantity() {
        return detailQuantity;
    }

    public void setDetailQuantity(String detailQuantity) {
        this.detailQuantity = detailQuantity;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<OrderHistoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<OrderHistoryModel> list) {
        this.list = list;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderTrackId() {
        return orderTrackId;
    }

    public void setOrderTrackId(String orderTrackId) {
        this.orderTrackId = orderTrackId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderTotalAmount() {
        return orderTotalAmount;
    }

    public void setOrderTotalAmount(String orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderShipName() {
        return orderShipName;
    }

    public void setOrderShipName(String orderShipName) {
        this.orderShipName = orderShipName;
    }

    public String getOrderShipAddress() {
        return orderShipAddress;
    }

    public void setOrderShipAddress(String orderShipAddress) {
        this.orderShipAddress = orderShipAddress;
    }

    public String getOrderPhone() {
        return orderPhone;
    }

    public void setOrderPhone(String orderPhone) {
        this.orderPhone = orderPhone;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
