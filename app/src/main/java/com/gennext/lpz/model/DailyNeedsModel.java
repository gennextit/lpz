package com.gennext.lpz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 11-Oct-16.
 */

public class DailyNeedsModel {

    // Main category entity
    private ArrayList<DailyNeedsModel> catList;
    private String output;
    private String outputMsg;


    private int catPosition;
    private String CategoryID;
    private String CategoryName;
    private String url;
    private String description;
    private Boolean isShowing;

    // Sub category entity
    private ArrayList<DailyNeedsModel> subCategoryList;
    private String outputSubCat;
    private String outputMsgSubCat;

    private int subCatPosition;
    private String subCategoryId;
    private String subCategoryName;
    private String categoryId;

    //Product entity
    private ArrayList<DailyNeedsModel> productlist;

    private int prodPosition;
    private String productID;
    private String productName;
    private String productWeight;
    private String productMRP;
    private String productRetailPrice;
    private String productShortDesc;
    private String productLongDesc;
    private String productImage;
    private String productStock;
    private String quantity;

    public Boolean getShowing() {
        return isShowing;
    }

    public void setShowing(Boolean showing) {
        isShowing = showing;
    }

    public int getProdPosition() {
        return prodPosition;
    }

    public void setProdPosition(int prodPosition) {
        this.prodPosition = prodPosition;
    }

    public int getCatPosition() {
        return catPosition;
    }

    public void setCatPosition(int catPosition) {
        this.catPosition = catPosition;
    }

    public int getSubCatPosition() {
        return subCatPosition;
    }

    public void setSubCatPosition(int subCatPosition) {
        this.subCatPosition = subCatPosition;
    }

    public ArrayList<DailyNeedsModel> getCatList() {
        return catList;
    }

    public void setCatList(ArrayList<DailyNeedsModel> catList) {
        this.catList = catList;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<DailyNeedsModel> getSubCategoryList() {
        return subCategoryList;
    }

    public void setSubCategoryList(ArrayList<DailyNeedsModel> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }

    public String getOutputSubCat() {
        return outputSubCat;
    }

    public void setOutputSubCat(String outputSubCat) {
        this.outputSubCat = outputSubCat;
    }

    public String getOutputMsgSubCat() {
        return outputMsgSubCat;
    }

    public void setOutputMsgSubCat(String outputMsgSubCat) {
        this.outputMsgSubCat = outputMsgSubCat;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<DailyNeedsModel> getProductlist() {
        return productlist;
    }

    public void setProductlist(ArrayList<DailyNeedsModel> productlist) {
        this.productlist = productlist;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }

    public String getProductMRP() {
        return productMRP;
    }

    public void setProductMRP(String productMRP) {
        this.productMRP = productMRP;
    }

    public String getProductRetailPrice() {
        return productRetailPrice;
    }

    public void setProductRetailPrice(String productRetailPrice) {
        this.productRetailPrice = productRetailPrice;
    }

    public String getProductShortDesc() {
        return productShortDesc;
    }

    public void setProductShortDesc(String productShortDesc) {
        this.productShortDesc = productShortDesc;
    }

    public String getProductLongDesc() {
        return productLongDesc;
    }

    public void setProductLongDesc(String productLongDesc) {
        this.productLongDesc = productLongDesc;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
