package com.gennext.lpz.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.lpz.R;

import java.util.ArrayList;

/**
 * Created by Abhijit on 29-Sep-16.
 */

public class CatAdapter extends ArrayAdapter<DailyNeedsModel> {
    private ArrayList<DailyNeedsModel> list;

    private Activity context;
//    public ImageLoader imageLoader;

    public CatAdapter(Activity context, int textViewResourceId, ArrayList<DailyNeedsModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
//        imageLoader = new ImageLoader(context.getApplicationContext());
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvCatName,tvDescription;
        ImageView ivCat;

        public ViewHolder(View v) {
            tvCatName = (TextView) v.findViewById(R.id.tv_slot_dn_cat_name);
//			tvDescription = (TextView) v.findViewById(R.id.tv_slot_dn_cat_description);
            ivCat = (ImageView) v.findViewById(R.id.iv_slot_dn_category);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_daily_needs2, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        DailyNeedsModel item = list.get(position);
        if (item.getCategoryName() != null) {
            holder.tvCatName.setText(item.getCategoryName());
        }
        if (item.getUrl() != null) {
            Glide.with(context)
                    .load(item.getUrl())
                    .placeholder(R.drawable.ic_error_loading_image)
                    .error(R.drawable.ic_error_loading_image)
                    .into(holder.ivCat);
        }
        return v;
    }

}
