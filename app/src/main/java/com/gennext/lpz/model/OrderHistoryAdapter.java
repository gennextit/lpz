package com.gennext.lpz.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.lpz.R;

import java.util.ArrayList;
import java.util.Random;

public class OrderHistoryAdapter extends ArrayAdapter<OrderHistoryModel> {
    private ArrayList<OrderHistoryModel> list;

    private Activity context;

    public OrderHistoryAdapter(Activity context, int textViewResourceId, ArrayList<OrderHistoryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }




    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvTrackId,tvTotalAmt,tvOrderDate,tvOrderTime,tvOrderStatus;
        LinearLayout slot;
        LinearLayout vTriangle;

        public ViewHolder(View v) {
            tvTrackId = (TextView) v.findViewById(R.id.tv_slot_order_track_id);
            tvTotalAmt = (TextView) v.findViewById(R.id.tv_slot_order_total_amt);
            tvOrderDate = (TextView) v.findViewById(R.id.tv_slot_order_date);
            tvOrderTime = (TextView) v.findViewById(R.id.tv_slot_order_time);
            tvOrderStatus = (TextView) v.findViewById(R.id.tv_slot_order_status);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
            vTriangle = (LinearLayout) v.findViewById(R.id.ll_triangle);

        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_order_history, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
//        setColor(holder.slot);

        if (list.get(position).getOrderTrackId() != null) {
            holder.tvTrackId.setText(list.get(position).getOrderTrackId());
        }
        if (list.get(position).getOrderTotalAmount() != null) {
            holder.tvTotalAmt.setText(list.get(position).getOrderTotalAmount());
        }
        if (list.get(position).getOrderDate() != null) {
            holder.tvOrderDate.setText(list.get(position).getOrderDate());
        }
        if (list.get(position).getOrderTime() != null) {
            holder.tvOrderTime.setText(list.get(position).getOrderTime());
        }
        String status =list.get(position).getStatus();

        if (status!= null) {
            holder.tvOrderStatus.setText(list.get(position).getStatus());
            if(status.equalsIgnoreCase("IN PROCESS")){
                holder.slot.setBackgroundColor(context.getResources().getColor(R.color.yellow));
                holder.vTriangle.setBackgroundResource(R.drawable.bg_triangle_yellow);
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.yellow));
            }else if(status.equalsIgnoreCase("CANCELLED")){
                holder.slot.setBackgroundColor(context.getResources().getColor(R.color.dark_red));
                holder.vTriangle.setBackgroundResource(R.drawable.bg_triangle_red);
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.dark_red));
            }else if(status.equalsIgnoreCase("RECEIVED")){
                holder.slot.setBackgroundColor(context.getResources().getColor(R.color.green));
                holder.vTriangle.setBackgroundResource(R.drawable.bg_triangle_green);
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.green));
            }
        }

        return v;
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }


}
