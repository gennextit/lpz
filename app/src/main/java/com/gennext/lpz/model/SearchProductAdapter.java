package com.gennext.lpz.model;


/**
 * Created by Abhijit on 14-Nov-16.
 */


import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.lpz.R;
import com.gennext.lpz.ShoppingActivity;
import com.gennext.lpz.shop.SearchProduct;
import com.gennext.lpz.util.DBManager;
import com.gennext.lpz.util.L;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// The standard text view adapter only seems to search from the beginning of whole words
// so we've had to write this whole class to make it possible to search
// for parts of the arbitrary string we want
public class SearchProductAdapter extends RecyclerView.Adapter<SearchProductAdapter.ReyclerViewHolder> {

    private final Activity context;
    private final SearchProduct searchProduct;
    private final LayoutInflater layoutInflater;
    private List<DailyNeedsModel> originalData;
    private List<DailyNeedsModel> filteredData;
    private ItemFilter mFilter = new ItemFilter();
    //    private int filterCriteria;
    int itemQuantiry = 0;
    //    DataTransferInterface dtInterface;
    DBManager db;

    public SearchProductAdapter(Activity context, List<DailyNeedsModel> data, SearchProduct searchProduct) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.filteredData = new ArrayList<>();
        this.originalData = data;
        this.searchProduct = searchProduct;
        db = new DBManager(context);
    }

    public int getItemCount() {
        if(filteredData!=null){
            return filteredData.size();
        }else{
            return 0;
        }
    }

    public DailyNeedsModel getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_search_product, parent, false);
        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final DailyNeedsModel item = filteredData.get(position);
        if (item.getProductName() != null) {
            holder.productName.setText(item.getProductName());
        }

        String retailPrice = item.getProductRetailPrice();
        String mrpPrice = item.getProductMRP();
        if (retailPrice != null) {
            holder.price.setText(retailPrice);
        }

        if (getInt(mrpPrice) <= getInt(retailPrice)) {
            holder.llRongPrice.setVisibility(View.GONE);
        } else {
            holder.llRongPrice.setVisibility(View.VISIBLE);
            holder.tvRongPrice.setText(mrpPrice);
        }

        if (item.getQuantity() != null) {
            holder.count.setText(item.getQuantity());
        }
        if (item.getProductImage() != null) {
            Glide.with(context)
                    .load(item.getProductImage())
                    .placeholder(R.drawable.ic_error_loading_image)
                    .error(R.drawable.ic_error_loading_image)
                    .into(holder.image);

        }

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(item.getQuantity());
                    itemQuantiry++;
                    float prodPrice = Float.parseFloat(item.getProductRetailPrice());
                    sumTotalPrice = prodPrice;
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }

                if (itemQuantiry != 0) {
                    item.setQuantity(String.valueOf(itemQuantiry));
                    replaceItemAt(item,position);
//                    replaceItemAt(position, item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(),
//                            item.getProductID(), item.getProductName(),
//                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage());
                    db.InsertProduct2(item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(), item.getProductID(), item.getProductName().replaceAll("'", "''"),
                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage().replaceAll("'", "''"));

                    int totalItem = ((ShoppingActivity) context).countTotalItem;
                    totalItem++;
                    ((ShoppingActivity) context).countTotalItem = totalItem;
                    ((ShoppingActivity) context).sumTotalPrice += sumTotalPrice;
                    ((ShoppingActivity) context).showCheckOutOption();
                    searchProduct.showCheckOutOption();
                }
            }
        });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(item.getQuantity());
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }


                if (itemQuantiry > 0) {
                    itemQuantiry--;

                    try {
                        sumTotalPrice = Float.parseFloat(item.getProductRetailPrice());
                    } catch (NumberFormatException e) {
                        L.m(e.toString());
                    }
                    item.setQuantity(String.valueOf(itemQuantiry));
                    replaceItemAt(item,position);
//                    replaceItemAt(position, item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(),
//                            item.getProductID(), item.getProductName(),
//                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage());
                    db.InsertProduct2(item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(), item.getProductID(), item.getProductName().replaceAll("'", "''"),
                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage().replaceAll("'", "''"));

                    ((ShoppingActivity) context).sumTotalPrice -= sumTotalPrice;
                    ((ShoppingActivity) context).countTotalItem--;
                    ((ShoppingActivity) context).showCheckOutOption();
                    searchProduct.showCheckOutOption();
                    if (itemQuantiry == 0) {
                        searchProduct.hideCheckOutOption();
                        ((ShoppingActivity) context).hideCheckOutOption();
                        db.DeleteProduct2(item.getCategoryId(), item.getSubCategoryId(), item.getProductID());

                    }
                }

            }
        });

    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView productName, unit, price, count, tvRongPrice;
        private ImageView image;//, plus, minus;
        private Button btnPlus, btnMinus;
        private LinearLayout llRongPrice;

        private ReyclerViewHolder(final View v) {
            super(v);
            productName = (TextView) v.findViewById(R.id.textView1);
            tvRongPrice = (TextView) v.findViewById(R.id.tv_rong_price);
            llRongPrice = (LinearLayout) v.findViewById(R.id.ll_main_price);
            unit = (TextView) v.findViewById(R.id.textView2);
            price = (TextView) v.findViewById(R.id.textView3);
            count = (TextView) v.findViewById(R.id.textView4);
            image = (ImageView) v.findViewById(R.id.imageView1);
            btnPlus = (Button) v.findViewById(R.id.btn_plus);
            btnMinus = (Button) v.findViewById(R.id.btn_minus);
        }
    }


    private int getInt(String mrpPrice) {
        int count = 0;
        try {
            count = Integer.parseInt(mrpPrice);
        } catch (NumberFormatException e) {
            count = 0;
        }
        return count;
    }


    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            final String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<DailyNeedsModel> list = originalData;

            int count = list.size();
            final ArrayList<DailyNeedsModel> nlist = new ArrayList<>(count);

            if (!filterString.equals("")) {
                for (int i = 0; i < list.size(); i++) {
                    List<DailyNeedsModel> subList = list.get(i).getSubCategoryList();
                    for (int j = 0; j < subList.size(); j++) {
                        List<DailyNeedsModel> productList = subList.get(j).getProductlist();
                        if(productList!=null)
                            for (DailyNeedsModel model : productList)
                                if (!filterString.equals("")) {
                                    if (model.getProductName() != null && model.getProductName().toLowerCase().contains(filterString)) {
                                        nlist.add(model);
                                    }
                                } else {
                                    nlist.add(model);
                                }
                    }
                }
                synchronized (this) {
                    results.values = nlist;
                    results.count = nlist.size();
                }
                return results;
            } else {
                synchronized (this) {
                    results.values = nlist;
                    results.count = nlist.size();
                }
                return results;
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, Filter.FilterResults results) {
            int count = results.count;
            filteredData = (ArrayList<DailyNeedsModel>) results.values;
            notifyDataSetChanged();
        }

        //        @Override
//        protected void publishResults(CharSequence constraint,
//                                      FilterResults results) {
//            int count = results.count;
//            filteredData = (ArrayList<DailyNeedsModel>) results.values;
//            notifyDataSetChanged();
//        }

    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }


    private void setSection(LinearLayout header, String label) {
        TextView text = new TextView(context);
        header.setBackgroundColor(0xffaabbcc);
        text.setTextColor(Color.WHITE);
        text.setText(label.substring(0, 1).toUpperCase());
        text.setTextSize(20);
        text.setPadding(5, 0, 0, 0);
        text.setGravity(Gravity.CENTER_VERTICAL);
        header.addView(text);
    }

    public void replaceItemAt(DailyNeedsModel item, int position) {
        // Replace the item in the array list
        filteredData.set(position, item);
        this.notifyDataSetChanged();
    }

    public void replaceItemAt(int slotPos, int prodPos, int catListPos, int subCatPos, String StoreId, String SubCatId, String id, String name, String price, String mrp,
                              int quantity, String imageSource) {
        // Replace the item in the array list
        DailyNeedsModel ob = new DailyNeedsModel();
        ob.setCatPosition(catListPos);
        ob.setSubCatPosition(subCatPos);
        ob.setProdPosition(prodPos);
        ob.setCategoryId(StoreId);
        ob.setSubCategoryId(SubCatId);
        ob.setProductID(id);
        ob.setProductName(name);
        ob.setProductMRP(mrp);
        ob.setProductRetailPrice(String.valueOf(price));
        ob.setQuantity(String.valueOf(quantity));
        ob.setProductImage(imageSource);
        this.filteredData.set(slotPos, ob);
        ArrayList<DailyNeedsModel> tempCatList = ((ShoppingActivity) context).dailyNeedsList;
        ArrayList<DailyNeedsModel> tempSubCatList = tempCatList.get(catListPos).getSubCategoryList();
        ArrayList<DailyNeedsModel> tempProductList = tempSubCatList.get(subCatPos).getProductlist();
        tempProductList.set(prodPos, ob);    // Let the custom adapter know it needs to refresh the view
        this.notifyDataSetChanged();
    }
}