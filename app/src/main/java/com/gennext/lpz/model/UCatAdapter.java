package com.gennext.lpz.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.lpz.R;
import com.gennext.lpz.ShoppingActivity;
import com.gennext.lpz.util.DBManager;
import com.gennext.lpz.util.L;

import java.util.ArrayList;

public class UCatAdapter extends ArrayAdapter<DailyNeedsModel> {
    private final ArrayList<DailyNeedsModel> list;
    //    private final int tabPos, subCatListPos;
    int itemQuantiry = 0;

    //    ImageLoader imageLoader;
    DBManager db;
    private final Activity context;
//    DataTransferInterface dtInterface;

    public UCatAdapter(Activity context, int textViewResourceId, ArrayList<DailyNeedsModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
//        imageLoader = new ImageLoader(context);
//        this.dtInterface = (DataTransferInterface) context;
        db = new DBManager(context);

    }

    class ViewHolder {
        TextView productName, unit, price, count, tvRongPrice;
        ImageView image;//, plus, minus;
        Button btnPlus, btnMinus;
        LinearLayout llRongPrice;

        public ViewHolder(View v) {
            productName = (TextView) v.findViewById(R.id.textView1);
            tvRongPrice = (TextView) v.findViewById(R.id.tv_rong_price);
            llRongPrice = (LinearLayout) v.findViewById(R.id.ll_main_price);
            unit = (TextView) v.findViewById(R.id.textView2);
            price = (TextView) v.findViewById(R.id.textView3);
            count = (TextView) v.findViewById(R.id.textView4);
            image = (ImageView) v.findViewById(R.id.imageView1);
//            plus = (ImageView) v.findViewById(R.id.imageView3);
//            minus = (ImageView) v.findViewById(R.id.imageView2);
            btnPlus = (Button) v.findViewById(R.id.btn_plus);
            btnMinus = (Button) v.findViewById(R.id.btn_minus);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_sub_category_large, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        final DailyNeedsModel item = list.get(position);

        if (item.getProductName() != null) {
            holder.productName.setText(item.getProductName());
        }
        String retailPrice = item.getProductRetailPrice();
        String mrpPrice = item.getProductMRP();
        if (retailPrice != null) {
            holder.price.setText(retailPrice);
        }

        if (getInt(mrpPrice) <= getInt(retailPrice)) {
            holder.llRongPrice.setVisibility(View.GONE);
        } else {
            holder.llRongPrice.setVisibility(View.VISIBLE);
            holder.tvRongPrice.setText(mrpPrice);
        }

        if (item.getQuantity() != null) {
            holder.count.setText(item.getQuantity());
        }
        if (item.getProductImage() != null) {
            Glide.with(context)
                    .load(item.getProductImage())
                    .placeholder(R.drawable.ic_error_loading_image)
                    .error(R.drawable.ic_error_loading_image)
                    .into(holder.image);

        }

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(item.getQuantity());
                    itemQuantiry++;
                    float prodPrice = Float.parseFloat(item.getProductRetailPrice());
                    sumTotalPrice = prodPrice;
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }

                if (itemQuantiry != 0) {

                    item.setQuantity(String.valueOf(itemQuantiry));
                    replaceItemAt(item, position);
//                    replaceItemAt(position,item.getProdPosition(), item.getCatPosition(),item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(),
//                            item.getProductID(), item.getProductName(),
//                            item.getProductRetailPrice(),item.getProductMRP(), itemQuantiry, item.getProductImage());
                    db.InsertProduct2(item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(), item.getProductID(), item.getProductName().replaceAll("'", "''"),
                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage().replaceAll("'", "''"));

                    ((ShoppingActivity) context).countTotalItem++;
                    ((ShoppingActivity) context).sumTotalPrice += sumTotalPrice;
                    ((ShoppingActivity) context).showCheckOutOption();

                }
            }
        });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(item.getQuantity());
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }


                if (itemQuantiry > 0) {
                    itemQuantiry--;

                    try {
                        sumTotalPrice = Float.parseFloat(item.getProductRetailPrice());
                    } catch (NumberFormatException e) {
                        L.m(e.toString());
                    }

                    item.setQuantity(String.valueOf(itemQuantiry));
                    replaceItemAt(item,position);

//                    replaceItemAt(position, item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(),
//                            item.getProductID(), item.getProductName(),
//                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage());
                    db.InsertProduct2(item.getProdPosition(), item.getCatPosition(), item.getSubCatPosition(), item.getCategoryId(), item.getSubCategoryId(), item.getProductID(), item.getProductName().replaceAll("'", "''"),
                            item.getProductRetailPrice(), item.getProductMRP(), itemQuantiry, item.getProductImage().replaceAll("'", "''"));

                    ((ShoppingActivity) context).sumTotalPrice -= sumTotalPrice;
                    ((ShoppingActivity) context).countTotalItem--;
                    ((ShoppingActivity) context).showCheckOutOption();
                    if (itemQuantiry == 0) {
                        ((ShoppingActivity) context).hideCheckOutOption();
                        db.DeleteProduct2(item.getCategoryId(), item.getSubCategoryId(), item.getProductID());
                    }
                }

            }
        });


        return v;
    }

    private int getInt(String mrpPrice) {
        int count = 0;
        try {
            count = Integer.parseInt(mrpPrice);
        } catch (NumberFormatException e) {
            count = 0;
        }
        return count;
    }

    public void replaceItemAt(DailyNeedsModel item, int position) {
        // Replace the item in the array list
        list.set(position, item);
        this.notifyDataSetChanged();
    }

    public void replaceItemAt(int slotPos, int prodPos, int catPos, int subCatPos, String StoreId, String SubCatId, String id, String name, String price, String mrp,
                              int quantity, String imageSource) {
        // Replace the item in the array list
        DailyNeedsModel ob = new DailyNeedsModel();
        ob.setCatPosition(catPos);
        ob.setSubCatPosition(subCatPos);
        ob.setProdPosition(prodPos);
        ob.setCategoryId(StoreId);
        ob.setSubCategoryId(SubCatId);
        ob.setProductID(id);
        ob.setProductName(name);
        ob.setProductMRP(mrp);
        ob.setProductRetailPrice(String.valueOf(price));
        ob.setQuantity(String.valueOf(quantity));
        ob.setProductImage(imageSource);
        this.list.set(slotPos, ob);
        ArrayList<DailyNeedsModel> tempCatList = ((ShoppingActivity) context).dailyNeedsList;
        ArrayList<DailyNeedsModel> tempSubCatList = tempCatList.get(catPos).getSubCategoryList();
        ArrayList<DailyNeedsModel> tempProductList = tempSubCatList.get(subCatPos).getProductlist();
        tempProductList.set(prodPos, ob);
//		AppTokens.globalList.set(position, ob);		// Let the custom adapter know it needs to refresh the view
        this.notifyDataSetChanged();
    }

}