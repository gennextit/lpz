package com.gennext.lpz.model;

/**
 * Created by Abhijit on 19-Nov-16.
 */

public class JsonModel {

    private String output;
    private String outputMsg;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
