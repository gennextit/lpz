package com.gennext.lpz.shop.bean;

public interface DataTransferInterface {
    public void setValues(int count, float sum);
}