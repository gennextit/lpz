package com.gennext.lpz.shop;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.gennext.lpz.R;
import com.gennext.lpz.ShoppingActivity;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.OrderHistoryAdapter;
import com.gennext.lpz.model.OrderHistoryModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppTokens;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 18-Oct-16.
 */

public class OrderHistory extends CompactFragment {
    AssignTask assignTask;
    private ListView lvMain;
    ArrayList<OrderHistoryModel> list;
    private int status;

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_order_history, container, false);
        setActionBar(v, "Order History");
        InitUI(v);
        return v;
    }

    public void setActionBar(View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(getActivity());
                        if (status == CHECKOUT_PROCESS) {
                            Intent intent = new Intent(getActivity(), ShoppingActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            getFragmentManager().popBackStack();
                        }
                    }
                }
        );

    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.expandableListView1);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (list != null) {
                    showPopupOrder(list.get(position).getOrderTrackId(), list.get(position).getOrderShipName()
                            , list.get(position).getOrderPhone(), list.get(position).getOrderShipAddress(), list.get(position).getList(), list.get(position).getStatus());
                }
            }
        });

        setProgressBar(v);
        if (!AppTokens.getSessionSignup(getActivity()).equals("")) {
            executeTask();
        } else {
            showPopupAlert("Alert", "Please update your profile to view order history", PopupAlert.FRAGMENT);
        }

    }

    protected void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_ORDER_LIST);
    }


    private void showPopupOrder(String orderTrackId, String orderShipName, String orderPhone, String orderShipAddress, ArrayList<OrderHistoryModel> list,String status) {
        OrderPopup orderPopup = new OrderPopup();
        orderPopup.setDetail(orderTrackId, orderShipName, orderPhone, orderShipAddress, list,status);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, orderPopup, "orderPopup");
        transaction.addToBackStack("orderPopup");
        transaction.commit();
    }


    private class AssignTask extends AsyncTask<String, Void, OrderHistoryModel> {
        Activity activity;
        private String response;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected OrderHistoryModel doInBackground(String... urls) {
            String mobile = AppUser.getMOBILE(activity);
            response = ApiCall.POST(urls[0], RequestBuilder.Mobile("customerMobile", mobile));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.getOrderHistoryInfo(response);
        }


        @Override
        protected void onPostExecute(OrderHistoryModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressBar(activity);
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        list = result.getList();
                        OrderHistoryAdapter adapter = new OrderHistoryAdapter(getActivity(), R.layout.slot_checkout, list);
                        lvMain.setAdapter(adapter);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            executeTask();
                        }
                    });
                }
            }
        }
    }
}