package com.gennext.lpz.shop;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.gennext.lpz.R;
import com.gennext.lpz.dialog.OrderCancelDialog;
import com.gennext.lpz.dialog.OrderConfirmDialog;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.model.OrderHistoryModel;
import com.gennext.lpz.model.OrderPopupAdapter;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class OrderPopup extends CompactFragment implements OrderConfirmDialog.OrderConfirmDialogListener,OrderCancelDialog.OrderCancelListener{
    private static final int ORDER_CONFIRM=1,ORDER_CANCEL=2;
    private static final String RECEIVED="RECEIVED",CANCELLED="CANCELLED";
    private TextView tvTrackId, tvName,tvMobile,tvAddress;
    private Button btnConfirm;
    private String orderTrackId,orderShipName,orderPhone,orderShipAddress;
    private FragmentManager manager;
    private ArrayList<OrderHistoryModel> orderList;
    private ListView lvMain;
    private Button btnCancel;

    AssignTask assignTask;
    private String orderStatus;
    private LinearLayout llButton;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void setDetail(String orderTrackId, String orderShipName, String orderPhone, String orderShipAddress, ArrayList<OrderHistoryModel> orderList, String status) {
        this.orderTrackId=orderTrackId;
        this.orderShipName=orderShipName;
        this.orderPhone=orderPhone;
        this.orderShipAddress=orderShipAddress;
        this.orderList=orderList;
        this.orderStatus=status;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_order, container, false);
        initToolBar(getActivity(),v, "Order Detail");
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTrackId = (TextView) v.findViewById(R.id.tv_popup_trackId);
        lvMain = (ListView) v.findViewById(R.id.lv_main);
        llButton = (LinearLayout) v.findViewById(R.id.llbutton);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.frag_popup_alert_order_header, null, false);
        tvName = (TextView) listHeaderView.findViewById(R.id.tv_popup_name);
        tvMobile = (TextView) listHeaderView.findViewById(R.id.tv_popup_mobile);
        tvAddress = (TextView) listHeaderView.findViewById(R.id.tv_popup_address);

        if(orderTrackId!=null)
            tvTrackId.setText("Order Track Id : "+orderTrackId);
        if(orderShipName!=null)
            tvName.setText(orderShipName);
        if(orderPhone!=null)
            tvMobile.setText(orderPhone);
        if(orderShipAddress!=null)
            tvAddress.setText(orderShipAddress);

        lvMain.addHeaderView(listHeaderView);

        if(orderList==null){
            orderList=new ArrayList<>();
        }
        OrderPopupAdapter adapter = new OrderPopupAdapter(getActivity(),R.layout.slot_checkout, orderList);
        lvMain.setAdapter(adapter);

        btnConfirm = (Button) v.findViewById(R.id.btn_popup1);
        btnCancel = (Button) v.findViewById(R.id.btn_popup2);

        if(orderStatus.equals(RECEIVED)){
            llButton.setVisibility(View.GONE);
        }else if(orderStatus.equalsIgnoreCase(CANCELLED)){
            llButton.setVisibility(View.GONE);
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderConfirmDialog.newInstance(orderTrackId,OrderPopup.this).
                        show(getFragmentManager(), "orderConfirmDialog");
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderCancelDialog.newInstance(orderTrackId,OrderPopup.this).
                        show(getFragmentManager(), "orderCancelDialog");
            }
        });
    }


    @Override
    public void onOrderConfirmClick(DialogFragment dialog,float rating,String feedback,String orderId) {
        executeTask(ORDER_CONFIRM,rating,feedback,null,orderId);
    }

    @Override
    public void onOrderCancelClick(DialogFragment dialog,String reason,String orderId) {
        executeTask(ORDER_CANCEL,0.0f,null,reason,orderId);
    }

    private void executeTask(int task,float rating,String feedback,String reason,String orderId) {
        if(task==ORDER_CONFIRM) {
            assignTask = new AssignTask(getActivity(),task,rating,feedback,reason,orderId);
            assignTask.execute(AppSettings.orderReceived);
        }else if(task==ORDER_CANCEL) {
            assignTask = new AssignTask(getActivity(), task,rating,feedback,reason,orderId);
            assignTask.execute(AppSettings.orderCancelled);
        }
    }

    private class AssignTask extends AsyncTask<String, Void, JsonModel> {
        private final float rating;
        private final String feedback,reason,orderId;
        private  int task;
        Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;

        }

        public AssignTask(Context context,int task,float rating,String feedback,String reason,String orderId) {
            this.rating=rating;
            this.feedback=feedback;
            this.reason=reason;
            this.task=task;
            this.orderId=orderId;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context,"Processing, Please wait.");
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response = null,mobile="";
            if(context!=null){
                mobile= AppUser.getMOBILE(context);
            }
            if(task==ORDER_CONFIRM) {
                response=ApiCall.POST(urls[0], RequestBuilder.OrderConfirm(rating,feedback,mobile,orderId));
            }else if(task==ORDER_CANCEL){
                response=ApiCall.POST(urls[0], RequestBuilder.OrderCancel(reason,mobile,orderId));
            }
            JsonParser jsonParser = new JsonParser();
            if(context!=null) {
                return jsonParser.getDefaultParser(response);
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if(context!=null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        OrderHistory orderHistory= (OrderHistory) getFragmentManager().findFragmentByTag("orderHistory");
                        if(orderHistory!=null){
                            orderHistory.executeTask();
                        }
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideBaseServerErrorAlertBox();
                            executeTask(task,rating,feedback,reason,orderId);
                        }
                    });
                }
            }
        }
    }
}