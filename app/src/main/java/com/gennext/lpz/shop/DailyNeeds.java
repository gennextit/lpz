package com.gennext.lpz.shop;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.ShoppingActivity;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.CatAdapter;
import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class DailyNeeds extends CompactFragment {
    public static final int OPEN=1,CLOSE=2;
    private GridView gridView;
    private ArrayList<DailyNeedsModel> catList;

    private LinearLayout llActionBack,llActionCart;
    private TextView tvTitle,tvCounter;
    private int countTotalItem;
    private CatAdapter catAdapter;
    private ArrayList<DailyNeedsModel> finalList;

    private AssignTask assignTask;
    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public void setListAndCounter(ArrayList<DailyNeedsModel> dailyNeeds,int countTotalItem) {
        this.countTotalItem=countTotalItem;
        this.catList=dailyNeeds;
    }

//    public void resetListAndCounter(ArrayList<DailyNeedsModel> dailyNeeds,int countTotalItem) {
//        this.countTotalItem=countTotalItem;
//        this.finalList=dailyNeeds;
//        if(catList!=null) {
//            catList.clear();
//            catList.addAll(finalList);
//            if(catAdapter!=null)
//                catAdapter.notifyDataSetChanged();
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_daily_needs, container, false);
        setActionBarOpt(v);
        InitUI(v);
        return v;
    }

    private void setActionBarOpt(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_menu_white);
//        toolbar.setNavigationOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        hideKeybord(getActivity());
//                        getFragmentManager().popBackStack();
//                    }
//                }
//
//        );
//        llActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        llActionCart = (LinearLayout) toolbar.findViewById(R.id.ll_actionbar_cart);
        tvCounter = (TextView) toolbar.findViewById(R.id.tv_subcat_count);
        ImageView ivSearch = (ImageView) toolbar.findViewById(R.id.iv_search);
        setSetDrawer(v,toolbar);
        setActionBarTitle(toolbar,getSt(R.string.app_name));

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ShoppingActivity)getActivity()).openSearchBox(catList,DailyNeeds.this);
            }
        });
        llActionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCheckOutPage();
            }
        });
    }

    private void openCheckOutPage() {
        Checkout checkout = new Checkout();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, checkout, "checkout");
        transaction.addToBackStack("checkout");
        transaction.commit();
    }

    private void setActionBarTitle(Toolbar toolbar,String title) {
        toolbar.setTitle(title);
        if(countTotalItem!=0){
            tvCounter.setText(String.valueOf(countTotalItem));
            llActionCart.setVisibility(View.VISIBLE);
        }else{
            llActionCart.setVisibility(View.GONE);
        }
    }



    public void setSetDrawer(View v, Toolbar toolbar) {
        SetDrawer(getActivity(),v,toolbar);
    }

    private void InitUI(View v) {
        setProgressBar(v);
        gridView = (GridView) v.findViewById(R.id.gridView1);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                if(catList!=null){
                    String catId=catList.get(position).getCategoryID();
                    String catName=catList.get(position).getCategoryName();
                    ArrayList<DailyNeedsModel> subCatList=catList.get(position).getSubCategoryList();
                    if(subCatList!=null&&subCatList.size()>0)
                        ((ShoppingActivity)getActivity()).openSubCategoty(catName,subCatList,DailyNeeds.this);
                }
            }
        });
        if(catList!=null){
            hideProgressBar();
            CatAdapter catAdapter = new CatAdapter(getActivity(),R.layout.slot_error, catList);
            gridView.setAdapter(catAdapter);
        }else{
            executeTask();
        }
    }


    private void executeTask() {
        assignTask =new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN);
    }

    public void updateCart(int countTotalItem) {
        this.countTotalItem=countTotalItem;
        if(this.countTotalItem !=0){
            tvCounter.setText(String.valueOf(this.countTotalItem));
            llActionCart.setVisibility(View.VISIBLE);
        }else{
            llActionCart.setVisibility(View.GONE);
        }
    }

    private class AssignTask extends AsyncTask<String, Void, DailyNeedsModel> {
        Context activity;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(activity!=null)
            showProgressBar();
        }

        @Override
        protected DailyNeedsModel doInBackground(String... urls) {
            String response = ApiCall.GET(urls[0]);
            JsonParser jsonParser = new JsonParser();
            if(activity!=null) {
                return jsonParser.getDailyNeedsInfo(activity,response);
            }else{
                return null;
            }
        }


        @Override
        protected void onPostExecute(DailyNeedsModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null) {
                hideProgressBar();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        catList = result.getCatList();
                        ((ShoppingActivity) getActivity()).setDailyNeedsList(catList);
                        catAdapter = new CatAdapter(getActivity(), R.layout.slot_error, catList);
                        gridView.setAdapter(catAdapter);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideBaseServerErrorAlertBox();
                            executeTask();
                        }
                    });
                }
            }

        }
    }


}
