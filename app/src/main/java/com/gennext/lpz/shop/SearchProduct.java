package com.gennext.lpz.shop;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.ShoppingActivity;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.model.SearchProductAdapter;
import com.gennext.lpz.util.AnimationClass;
import com.gennext.lpz.util.AppAnimation;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;

import java.util.ArrayList;

/**
 * Created by Abhijit on 12-Nov-16.
 */

public class SearchProduct extends CompactFragment {

    AssignTask assignTask;
    private RecyclerView lvMain;
    private EditText etSearch;
    private SearchProductAdapter adapter;
    private AnimationClass anim;
    private ArrayList<DailyNeedsModel> catList;
    private SearchProduct fragment;
    private LinearLayout Checkout;
    private TextView tvCounter, tvSumPrice;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static SearchProduct newInstance(ArrayList<DailyNeedsModel> catList) {
        SearchProduct fragment = new SearchProduct();
        fragment.catList = catList;
        fragment.fragment = fragment;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_search_product, container, false);
        initToolSearchBar(v);
        anim = new AnimationClass();
        InitUI(v);
        return v;
    }

    private void initToolSearchBar(View v) {
        LinearLayout llBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        etSearch = (EditText) v.findViewById(R.id.et_search);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                getFragmentManager().popBackStack();
                ((ShoppingActivity) getActivity()).attachDailyNeedsList();
            }
        });
    }

    private void InitUI(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.rv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        if (catList != null) {
            adapter = new SearchProductAdapter(getActivity(), catList, SearchProduct.this);
            lvMain.setAdapter(adapter);
        }

        setProgressBar(v);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(adapter!=null)
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        Checkout = (LinearLayout) v.findViewById(R.id.llcheckout);
        tvCounter = (TextView) v.findViewById(R.id.tv_subcat_count);
        tvSumPrice = (TextView) v.findViewById(R.id.tv_subcat_price);
        Checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                openCheckOutPage();
            }
        });
        showCheckOutOption();
        hideCheckOutOption();
        hideProgressBar();
//        executeTask();
    }

    public void clearSearch() {
        etSearch.setText("");
    }


    private void openCheckOutPage() {
        Checkout checkout = new Checkout();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, checkout, "checkout");
        transaction.addToBackStack("checkout");
        transaction.commit();
        clearSearch();
        hideKeybord(getActivity());
    }

    public void hideCheckOutOption() {
        hideKeybord(getActivity());
        if (((ShoppingActivity) getActivity()).countTotalItem == 0) {
            Checkout.setVisibility(View.GONE);
        }
    }

    public void showCheckOutOption() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                hideKeybord(getActivity());
                int countTotalItem = ((ShoppingActivity) getActivity()).countTotalItem;
                float sumTotalPrice = ((ShoppingActivity) getActivity()).sumTotalPrice;
                //Do something on UiThread
                tvCounter.setText(String.valueOf(countTotalItem));
                tvSumPrice.setText(String.format("%.2f", sumTotalPrice));
                Checkout.setVisibility(View.VISIBLE);
            }
        });
    }

    public void showCheckOutOption(final float sumTotalPrice) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                hideKeybord(getActivity());
                int countTotalItem = ((ShoppingActivity) getActivity()).countTotalItem;
                //Do something on UiThread
                tvCounter.setText(String.valueOf(countTotalItem));
                tvSumPrice.setText(String.format("%.2f", sumTotalPrice));
                Checkout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN);
    }

//    private void setSearchCriteria(int criteria) {
//        adapter.setFilterCriteria(criteria);
//    }


    private class AssignTask extends AsyncTask<String, Void, DailyNeedsModel> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected DailyNeedsModel doInBackground(String... urls) {
            DailyNeedsModel model;
            String response;

            return null;
        }

        @Override
        protected void onPostExecute(DailyNeedsModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if (result.getOutput().equals("success")) {
                    catList.addAll(result.getCatList());
                    adapter.notifyDataSetChanged();
                    lvMain.setAdapter(adapter);
                } else {
                    showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                }
            } else {
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask();
                    }
                });
            }
        }
    }


//    public void showFilterAlertBox(Activity activity) {
//
//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
//
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = activity.getLayoutInflater();
//
//        View v = inflater.inflate(R.layout.alert_filter_dialog, null);
//        dialogBuilder.setView(v);
//        ListView lvDialog = (ListView) v.findViewById(R.id.lv_filter);
//        lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
//                dialog.dismiss();
//                setSearchCriteria(pos);
//            }
//        });
//
//        dialog = dialogBuilder.create();
//        dialog.show();
//
//    }

}