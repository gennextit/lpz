/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gennext.lpz.shop.bean;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import com.gennext.lpz.R;
import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.model.UCatAdapter;
import com.gennext.lpz.ShoppingActivity;

import java.util.ArrayList;

public class SuperAwesomeCardFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    private int position;
    private ArrayList<DailyNeedsModel> aList;
    private UCatAdapter adapter;
//    private Context activity;
    private ListView lv;

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        this.activity = context;
//    }

    public static SuperAwesomeCardFragment newInstance(int position) {
        SuperAwesomeCardFragment f = new SuperAwesomeCardFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getArguments().getInt(ARG_POSITION);
//		if(AppTokens.globalList!=null){
//			adapter = new UCatAdapter(getActivity(), R.layout.custom_ucat_slot, AppTokens.globalList);
//		}else{
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_sub_cat, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lv = (ListView) v.findViewById(R.id.lv_rwa);
//        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//
//        FrameLayout fl = new FrameLayout(getActivity());
//        fl.setLayoutParams(params);
//
//        final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
//                getResources().getDisplayMetrics());
//
//        ListView lv = new ListView(getActivity());
//        params.setMargins(margin, margin, margin, margin);
//        lv.setLayoutParams(params);
//        lv.setDivider(null);

        aList = ((ShoppingActivity) getActivity()).subCatList;
        aList=aList.get(position).getProductlist();
        if(aList!=null && aList.size()!=0){
            adapter = new UCatAdapter(getActivity(), R.layout.slot_sub_category, aList);
            lv.setAdapter(adapter);
        }else{
            ArrayList<String> errorList = new ArrayList<>();
            errorList.add("Product not available under this category");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_error,
                    R.id.tv_message, errorList);
            lv.setAdapter(adapter);
        }



//        fl.addView(lv);
//        return fl;
    }

//    private class LoadSubCat extends AsyncTask<String, Void, String> {
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			// progressBar.setVisibility(View.VISIBLE);
//		}
//
//		@Override
//		protected String doInBackground(String... params) {
//
//			String response = "error";
//			HttpReq req = new HttpReq();
//			response = LoadPref(params[0]);
//			AppTokens.globalList=new ArrayList<UCatModel>();
//			if (response.contains("[")) {
//				try {
//					JSONArray data = new JSONArray(response);
//					for (int i = 0; i < data.length(); i++) {
//						JSONObject obj = data.getJSONObject(i);
//						if (obj.optString("subcat_id").equalsIgnoreCase(String.valueOf(position + 1))) {
//
//							JSONArray subCat = obj.getJSONArray("u_subcat_details");
//							for (int j = 0; j < subCat.length(); j++) {
//								JSONObject dt = subCat.getJSONObject(j);
//								UCatModel ob = new UCatModel();
//								ob.setStore_category_id(dt.optString("store_category_id"));
//								ob.setSubcat_id(dt.optString("subcat_id"));
//								ob.setU_subcat_id(dt.optString("u_subcat_id"));
//								ob.setU_subcat_name(dt.optString("u_subcat_name"));
//								ob.setU_subcat_price(dt.optString("u_subcat_price"));
//								ob.setU_subcat_quantity("0");
//								if(dt.optString("subcat_id").equalsIgnoreCase("1")&&dt.optString("store_category_id").equalsIgnoreCase("1")){
//									ob.setDrawableSource(vegImage[j]);
//								}else if(dt.optString("subcat_id").equalsIgnoreCase("2")&&dt.optString("store_category_id").equalsIgnoreCase("1")){
//									ob.setDrawableSource(fruitImage[j]);
//								}else{
//									ob.setDrawableSource(R.drawable.bg_load);
//								}
//								aList.add(ob);
//								AppTokens.globalList.add(ob);
//							}
//							response = "success";
//						} else if (obj.optString("status").equalsIgnoreCase("failure")) {
//							response = "failure";
//						}
//					}
//
//				} catch (JSONException e) {
//					L.m(e.toString());
//				}
//			} else {
//				L.m("Invalid JSON found : " + response);
//				return response;
//			}
//
//			return response;
//
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			// progressBar.setVisibility(View.GONE);
//			final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4,
//					getResources().getDisplayMetrics());
//			switch (result) {
//			case "success":
//
//				adapter.notifyDataSetChanged();
//
//				break;
//			case "failure":
//
//				adapter.notifyDataSetChanged();
//
//				break;
//			case "error":
//				// showAlertInternet(LogIn.this,result);
//				L.m("" + result);
//				break;
//			default:
//				// showAlertInternet(LogIn.this,result);
//				L.m("" + result);
//
//			}
//
//		}
//	}
//
//	public String LoadPref(String key) {
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
//		String data = sharedPreferences.getString(key, "");
//		return data;
//	}

}