package com.gennext.lpz.shop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.CheckoutJson;
import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.DBManager;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class CheckoutConfirm extends CompactFragment {
    private EditText etName,etCoupon;
    private TextView tvAddress,tvMobile,tvPayable,tvTotal,tvShipping;
    private ProgressDialog pDialog;
    private DBManager db;

    LinearLayout llActionBack;
    TextView tvTitle;
    private String payableAmount;
    private float shipingAmount=0.0f;
    private LinearLayout llAddress;
    LoadCheckoutList loadCheckoutList;
    private String name,mobile,address,alternateMobile;
    private RadioGroup rgTypeOfOrder;
    private RadioButton radioSexButton;
    private String typeOfOrder,typeOfOrderMsg;

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if(loadCheckoutList!=null){
            loadCheckoutList.onAttach(context);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(loadCheckoutList!=null){
            loadCheckoutList.onDetach();
        }

    }

    public void setCheckoutDetail(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_checkout_confirm, container, false);
        setActionBarOpt(v);
        InitUI(v);
        return v;
    }

    private void setActionBarOpt(View v) {
        llActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) v.findViewById(R.id.actionbar_title);
        setActionBarOption();
        setActionBarTitle("Delivery Details");

    }
    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getFragmentManager().popBackStack();
            }
        });
    }


    private void InitUI(View v) {
        etName = (EditText) v.findViewById(R.id.et_delivery_name);
        etCoupon = (EditText) v.findViewById(R.id.et_delivery_couponCode);
        tvAddress = (TextView) v.findViewById(R.id.tv_delivery_address);
        tvMobile = (TextView) v.findViewById(R.id.tv_delivery_mobile);
        tvPayable = (TextView) v.findViewById(R.id.tv_delivery_payableAmt);
        tvTotal = (TextView) v.findViewById(R.id.tv_delivery_totalAmt);
        tvShipping = (TextView) v.findViewById(R.id.tv_delivery_shipingAmt);
        llAddress = (LinearLayout) v.findViewById(R.id.llAddress);
        rgTypeOfOrder = (RadioGroup) v.findViewById(R.id.rg_order_type);

        Button btnConfirm=(Button)v.findViewById(R.id.btn_checkout_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmOrderClick();
            }
        });
        llAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddressOption();
            }
        });

        address= AppUser.getAddress(getActivity());
        mobile=AppUser.getMOBILE(getActivity());
        name=AppUser.getName(getActivity());
        float TotalPrice= Float.parseFloat(payableAmount);

        if(!address.equals("")){
            tvAddress.setText(address);
        }
        if(!mobile.equals("")){
            tvMobile.setText(mobile);
        }
        if(!name.equals("")){
            etName.setText(name);
        }
        tvPayable.setText(String.format("%.2f",(TotalPrice+shipingAmount)));
        tvTotal.setText(String.format("%.2f",TotalPrice));
        tvShipping.setText(String.valueOf(shipingAmount));
        
        db = new DBManager(getActivity());
        typeOfOrder="P";
        typeOfOrderMsg=getSt(R.string.checkout_partial);
        rgTypeOfOrder.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if(checkedId==R.id.rb_order_type1){
                    typeOfOrder="P";
                    typeOfOrderMsg=getSt(R.string.checkout_partial);
                    showPopupAlert("Alert",typeOfOrderMsg,PopupAlert.POPUP_DIALOG);
                }else if(checkedId==R.id.rb_order_type2){
                    typeOfOrder="C";
                    typeOfOrderMsg=getSt(R.string.checkout_complete);
                    showPopupAlert("Alert",typeOfOrderMsg,PopupAlert.POPUP_DIALOG);
                }
            }
        });
    }

     
    
    public void onConfirmOrderClick() {
        loadCheckoutList=new LoadCheckoutList(getActivity(),tvAddress.getText().toString(),tvMobile.getText().toString(),
                tvPayable.getText().toString(),etName.getText().toString(),typeOfOrder);
        loadCheckoutList.execute(AppSettings.ORDER);
    }




    private class LoadCheckoutList extends AsyncTask<String, Void, JsonModel> {

        private String address,mobile,payableAmt,TOO;
        private Activity activity;
        private String personName;

        public void onAttach(Activity activity) {
            this.activity=activity;
        }
        public void onDetach() {
            this.activity=null;
        }

        private LoadCheckoutList(Activity activity, String address, String mobile, String payableAmt, String personName,String too){
            this.activity=activity;
            this.address=address;
            this.mobile=mobile;
            this.payableAmt=payableAmt;
            this.personName=personName;
            this.TOO=too;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity,"Processing, Keep patience!");
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String checkOutJson="",response = null,regMobile="";
            if(activity!=null){
                regMobile=AppUser.getMOBILE(activity);
                ArrayList<DailyNeedsModel> checkoutlist;
                checkoutlist=db.ViewData2();
                if (checkoutlist!=null) {
                    checkOutJson= CheckoutJson.toJSonArray(checkoutlist);
                }
                response= ApiCall.POST(urls[0], RequestBuilder.Order(regMobile,checkOutJson,payableAmt,address,mobile,personName,TOO));
                JsonParser jsonParser=new JsonParser();
                return jsonParser.parseCheckoutConfirmResponse(response);
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null){
                hideProgressDialog();
                if (result != null) {
                    if(result.getOutput().equals("success")){
                        db.DropTable();
                        showPopupAlert("Alert",result.getOutputMsg()+"\n"+typeOfOrderMsg, PopupAlert.CHECKOUT_CONFIRM);
                    }else if(result.getOutput().equals("failure")){
                        showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }else{
                        showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }
                }else{
                    Button retry=showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            onConfirmOrderClick();
                        }
                    });
                }
            }
        }
    }

     
//    public class fakeConfirmOrder extends AsyncTask<Void, Void, Void> {
//
//        private ProgressDialog pDialog;
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            pDialog = new ProgressDialog(getActivity());
//            pDialog.setCancelable(false);
//            pDialog.setMessage("Processing, Keep patience!");
//            pDialog.show();
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            // TODO Auto-generated method stub
//
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            pDialog.dismiss();
//            db.DropTable();
//            showPopupAlert("Alert","Your Order Confirmed successfully", PopupAlert.FRAGMENT);
////            Toast.makeText(getActivity(),"Order Confirmed successfully",Toast.LENGTH_SHORT).show();
////            showToast();
////            AppTokens.sumTotalPrice=0;
////            AppTokens.countTotalItem=0;
//
////            getActivity().finish();
//
//        }
//
//    }


    public void showAddressOption() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_address_change, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final EditText etAddress = (EditText) v.findViewById(R.id.et_alert_address);
        etAddress.setText(address);
        final EditText etMobile = (EditText) v.findViewById(R.id.et_alert_mobile);


        button1.setText("OK");
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                String address=etAddress.getText().toString();
                if(!address.equals(""))
                tvAddress.setText(address);
                alternateMobile=etMobile.getText().toString();
                tvMobile.setText(mobile+"\n"+alternateMobile);
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
    }
}
