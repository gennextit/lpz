package com.gennext.lpz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.shop.Checkout;
import com.gennext.lpz.shop.DailyNeeds;
import com.gennext.lpz.shop.SearchProduct;
import com.gennext.lpz.shop.bean.PagerSlidingTabStrip;
import com.gennext.lpz.shop.bean.SuperAwesomeCardFragment;
import com.gennext.lpz.util.DBManager;

import java.util.ArrayList;


public class ShoppingActivity extends BaseActivity {
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private MyPagerAdapter adapter;
    public ArrayList<DailyNeedsModel> subCatList;
    private LinearLayout Checkout;
    public int countTotalItem = 0;
    public float sumTotalPrice = 0.0f;
    private TextView tvCounter, tvSumPrice;
    private ImageView ivRupee;
    private String catId, catName, subCatJson;
    FragmentManager manager;
    LinearLayout llActionBack;
    TextView tvTitle;
    public ArrayList<DailyNeedsModel> dailyNeedsList;
    private ArrayList<DailyNeedsModel> finalList;
    int dailyNeedsStatus = CLOSE;
    private LinearLayout llProgress;
    private DBManager db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcat);
        db = new DBManager(this);
        manager = getSupportFragmentManager();
        Intent intent=getIntent();
        if(intent!=null) {
            catId = intent.getStringExtra("catId");
            catName = intent.getStringExtra("catName");
            subCatJson = intent.getStringExtra("subCatList");

//            int data=intent.getIntExtra(AppTokens.PROFILE_STATUS,0);
//            if(data==RESULT_OK){
//
//            }
        }
        initUi();
    }

    private void openCheckoutConfirmScreen() {
        Checkout checkout = (Checkout) manager.findFragmentByTag("checkout");
        if (checkout != null) {
            checkout.onConfirmOrderClick();
        }
    }

    private void initUi() {
        setActionBarOpt();
        llProgress = (LinearLayout) findViewById(R.id.ll_progress);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        tvCounter = (TextView) findViewById(R.id.tv_subcat_count);
        tvSumPrice = (TextView) findViewById(R.id.tv_subcat_price);
        ivRupee = (ImageView) findViewById(R.id.iv_subcat_rupeeLogo);
        Checkout = (LinearLayout) findViewById(R.id.llcheckout);
        Checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                openCheckOutPage();
//                if(!LoadPref(AppTokens.Mobile).equals("")){
//                    Intent ob=new Intent(SubCategoryActivity.this,CheckoutActivity.class);
//                    startActivity(ob);
//
//                }else{
//                    Intent ob=new Intent(SubCategoryActivity.this,LogIn.class);
//                    ob.putExtra("from", "subcat");
//                    startActivity(ob);
//                }
            }
        });
        hideCheckOutOption();
        showDailyNeedsList();
    }

    private void openCheckOutPage() {
        com.gennext.lpz.shop.Checkout checkout = new Checkout();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, checkout, "checkout");
        transaction.addToBackStack("checkout");
        transaction.commit();
    }

    public void hideCheckOutOption() {
        if(countTotalItem==0) {
            Checkout.setVisibility(View.GONE);
        }
    }

    public void showCheckOutOption() {
        runOnUiThread(new Runnable() {
            public void run() {
                //Do something on UiThread
                tvCounter.setText(String.valueOf(countTotalItem));
                tvSumPrice.setText(String.format("%.2f", sumTotalPrice));
                Checkout.setVisibility(View.VISIBLE);
                SearchProduct searchProduct = (SearchProduct) manager.findFragmentByTag("searchProduct");
                if (searchProduct != null) {
                    searchProduct.showCheckOutOption();
                }
                DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
                if (dailyNeeds != null) {
                    dailyNeeds.updateCart(countTotalItem);
                }
            }
        });
    }

    private void setActionBarOpt() {
        llActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) findViewById(R.id.actionbar_title);
        setActionBarOption();

    }

    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
                if (dailyNeeds != null) {
                    dailyNeedsStatus = OPEN;
                    dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.attach(dailyNeeds);
                    transaction.commit();
                }
            }
        });
    }




    public void attachDailyNeedsList() {
        DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
        if (dailyNeeds != null) {
            dailyNeedsStatus = OPEN;
            dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.attach(dailyNeeds);
            transaction.commit();
        }
    }

    private void showDailyNeedsList() {
        dailyNeedsStatus = OPEN;
        DailyNeeds dailyNeeds = new DailyNeeds();
        dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, dailyNeeds, "dailyNeeds");
        transaction.addToBackStack("dailyNeeds");
        transaction.commit();
    }


    public void openSearchBox(ArrayList<DailyNeedsModel> catList, DailyNeeds dailyNeeds) {
        if (dailyNeeds != null) {
            dailyNeedsStatus = CLOSE;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.detach(dailyNeeds);
            transaction.commit();
        }
        addFragment(SearchProduct.newInstance(catList),"searchProduct");
    }

    public void openSubCategoty(String catName, ArrayList<DailyNeedsModel> subCatList, DailyNeeds dailyNeeds) {
        setActionBarTitle(catName);
        this.subCatList=subCatList;
        if (dailyNeeds != null) {
            dailyNeedsStatus = CLOSE;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.detach(dailyNeeds);
            transaction.commit();
        }
        setListWithTabs(subCatList);
    }



    public void setListWithTabs(ArrayList<DailyNeedsModel> subCatList) {

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        adapter = new MyPagerAdapter(getSupportFragmentManager(), subCatList);
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        pager.setPageMargin(pageMargin);
        tabs.setTextColor(getResources().getColor(R.color.white));
        tabs.setUnderlineColor(getResources().getColor(R.color.background_window));
        tabs.setUnderlineHeight(1);
        tabs.setIndicatorColor(getResources().getColor(R.color.white));
        tabs.setIndicatorHeight(6);
        tabs.setAllCaps(false);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);
    }

    public void refreshList(final DailyNeedsModel item){
        if(subCatList!=null) {

            new Thread(new Runnable() {
                public void run() {
                    // a potentially  time consuming task
                    for (DailyNeedsModel cat:dailyNeedsList){
                        if(item.getCategoryId().equalsIgnoreCase(cat.getCategoryID())){
                            ArrayList<DailyNeedsModel> subCatItems = cat.getSubCategoryList();
                            for (DailyNeedsModel subCat : subCatItems){
                                if(item.getSubCategoryId().equalsIgnoreCase(subCat.getSubCategoryId())){
                                    if(subCat.getProductlist()!=null)
                                    for (DailyNeedsModel prod : subCat.getProductlist()) {
                                        if (item.getProductID().equalsIgnoreCase(prod.getProductID())) {
                                            prod.setQuantity(item.getQuantity());
                                        }
                                    }
                                }
                            }
                        }
                    }

//                    for(DailyNeedsModel subCat : subCatList){
//                        if(item.getSubCategoryId().equalsIgnoreCase(subCat.getSubCategoryId())){
//                            for (int i=0; i< subCat.getProductlist().size();i++) {
//                                if (item.getProductID().equalsIgnoreCase(subCat.getProductlist().get(i).getProductID())) {
////                                    prod.setQuantity(item.getQuantity());
//                                    subCatList.set(i,item);
//                                }
//                            }
//                        }
//                    }
                    pager.post(new Runnable() {
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }).start();

        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        openCheckoutConfirmScreen();
    }

    public void setDailyNeedsList(ArrayList<DailyNeedsModel> list) {
        this.dailyNeedsList = list;
        this.finalList=list;
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        ArrayList<DailyNeedsModel> subCatList;

        public MyPagerAdapter(FragmentManager fm, ArrayList<DailyNeedsModel> subCatList) {
            super(fm);
            this.subCatList = subCatList;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return subCatList.get(position).getSubCategoryName();
        }

        @Override
        public int getCount() {
            return subCatList.size();
            //return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            return SuperAwesomeCardFragment.newInstance(position);
        }


    }




    @Override
    public void onBackPressed() {
        int count=manager.getBackStackEntryCount();
        if ( count > 0) {
            if(count>=2){
                if(getSupportFragmentManager().findFragmentByTag("checkout")!=null){
                    attachDailyNeedsList();
                }
                manager.popBackStack();
            }else{
                if (dailyNeedsStatus == CLOSE) {
                    updateMainList();
                } else {
                    finish();
                }
            }
        } else {
            super.onBackPressed();
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.DropTable();
    }

    public void updateMainList() {
        DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
        if (dailyNeeds != null) {
            dailyNeedsStatus = OPEN;
            dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.attach(dailyNeeds);
            transaction.commit();
        }
    }

    public void resetMainList() {
        DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
        if (dailyNeeds != null) {
            dailyNeedsStatus = OPEN;
            dailyNeedsList=finalList;
            countTotalItem=0;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(android.R.id.content,dailyNeeds,"dailyNeeds");
            transaction.commit();
//            dailyNeeds.resetListAndCounter(finalList,0);

        }
    }
}
