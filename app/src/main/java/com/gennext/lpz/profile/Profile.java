package com.gennext.lpz.profile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gennext.lpz.ProfileActivity;
import com.gennext.lpz.R;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.FieldValidation;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class Profile extends CompactFragment implements View.OnFocusChangeListener {
    public static final int CHECKOUT_SCREEN=1,DRAWER_SCREEN=2;
    private String[] profile = new String[4];
    private AssignTask assignTask;
    private EditText etName, etEmail, etPin, etAddress;
    private Button btnSubmit;
    private ProgressBar progressBar;
    private String floor;
    private String Colony;
    private String address;
    private int openStatus;
//    private BroadcastReceiver mHandleMessageReceiver;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_update_profile, container, false);
        initToolBar(getActivity(),v,"Address Detail");
        InitUI(v);
        return v;
    }



    private void InitUI(View v) {
//        spFloor = (Spinner) v.findViewById(R.id.sp_update_profile_floor);
        etName = (EditText) v.findViewById(R.id.et_update_profile_name);
        etEmail = (EditText) v.findViewById(R.id.et_update_profile_email);
        etPin = (EditText) v.findViewById(R.id.et_update_profile_pin);
        etAddress = (EditText) v.findViewById(R.id.et_update_profile_address);
        btnSubmit = (Button) v.findViewById(R.id.btn_update_profile);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        etName.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);
        etPin.setOnFocusChangeListener(this);
        etAddress.setOnFocusChangeListener(this);

        profile = AppUser.getProfileData(getActivity());
        etName.setText(profile[0]);
        if (!profile[0].equals("")) {
            etName.requestFocus();
        }
        etEmail.setText(profile[1]);
        if (!profile[1].equals("")) {
            etEmail.requestFocus();
        }

        etPin.setText(profile[2]);
        if (!profile[2].equals("")) {
            etPin.requestFocus();
        }
        address = profile[3];
        etAddress.setText(address != null ? address : "");
        if (!address.equals("")) {
            etAddress.requestFocus();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etName.requestFocus();
                etEmail.requestFocus();
                etPin.requestFocus();
                etAddress.requestFocus();
                hideKeybord(getActivity());
                executeTask();
            }
        });
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (view == etName) {
            if (!hasFocus) {
                FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true);
            }
        } else if (view == etEmail) {
            if (!hasFocus) {
                FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true);
            }
        } else if (view == etPin) {
            if (!hasFocus) {
                FieldValidation.validate(getActivity(), etPin, FieldValidation.PIN_CODE, true);
            }
        } else if (view == etAddress) {
            if (!hasFocus) {
                FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, false);
            }
        }
//        else if (view == etAddress) {
//            if (!hasFocus) {
//                FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, false);
//            }
//        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true)) {
            return false;
        }else if (!FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return false;
        }else if (!FieldValidation.validate(getActivity(), etPin, FieldValidation.PIN_CODE, true)) {
            return false;
        }else if (!FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, false)) {
            return false;
        }
        return true;
    }

    private void executeTask() {
        if (checkValidation()) {
            assignTask = new AssignTask(getActivity(), etName.getText().toString()
                    , etEmail.getText().toString(), etPin.getText().toString(), etAddress.getText().toString());
            assignTask.execute(AppSettings.UPDATE_PROFILE);
        } else {
            Toast.makeText(getActivity(), "Please fill mandatory fields.", Toast.LENGTH_LONG).show();
        }
    }

    public void setOpenStatus(int openStatus) {
        this.openStatus = openStatus;
    }




    private class AssignTask extends AsyncTask<String, Void, JsonModel> {
        Activity activity;
        private String name, email, pin, address;
        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String Name, String Email, String pin, String Address) {
            this.activity = activity;
            this.name = Name;
            this.email = Email;
            this.pin = pin;
            this.address = Address;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity,"Updating profile, Please wait.");
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response;
            String mobile = "";
            if (activity != null) {
                mobile = AppUser.getMOBILE(activity);
            }
            response = ApiCall.POST(urls[0], RequestBuilder.UpdateProfile(mobile, name, email, pin, address));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseUpdateProfileJson(response);
        }


        @Override
        protected void onPostExecute(JsonModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null) {
                hideProgressDialog();
//                removeCProgress(cProgress,btnSubmit);
                if (result != null) {
                    if (result.getOutput().equalsIgnoreCase("success")) {
                        AppUser.setUserProfile(activity, name, email, pin, address);
                        if (openStatus == CHECKOUT_SCREEN) {
                            ((ProfileActivity) getActivity()).onSuccessUpdatedProfile();
                        } else {
                            Toast.makeText(getActivity(), "Profile updated", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideBaseServerErrorAlertBox();
                            executeTask();
                        }
                    });
                }
            }
        }

    }




    //        mHandleMessageReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                // checking for type intent filter
//                if (intent.getAction().equals(GCMTokens.REGISTRATION_COMPLETE)) {
//                    // gcm successfully registered
//                    // now subscribe to `global` topic to receive app wide notifications
////                    FirebaseMessaging.getInstance().subscribeToTopic(GCMTokens.TOPIC_GLOBAL);
//
//                    displayFirebaseRegId();
//
//                } else if (intent.getAction().equals(GCMTokens.PUSH_NOTIFICATION)) {
//                    // new push notification is received
//
//                    String message = intent.getStringExtra("message");
//
//                    Toast.makeText(getActivity(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//
//                }
//            }
//        };


}

//    private void displayFirebaseRegId() {
//        String token = FirebaseInstanceId.getInstance().getToken();
//        if (token != null && !token.equals("")) {
//            GCMTokens.setGCM(getActivity(), token);
////            Toast.makeText(getActivity(), "Push notification: " + token, Toast.LENGTH_LONG).show();
//
//        }
//    }


//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        // register GCM registration complete receiver
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mHandleMessageReceiver,
//                new IntentFilter(GCMTokens.REGISTRATION_COMPLETE));
//
//        // register new push message receiver
//        // by doing this, the activity will be notified each time a new message arrives
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mHandleMessageReceiver,
//                new IntentFilter(GCMTokens.PUSH_NOTIFICATION));
//
//        // clear the notification area when the app is opened
//        NotificationUtils.clearNotifications(getActivity());
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mHandleMessageReceiver);
//    }


