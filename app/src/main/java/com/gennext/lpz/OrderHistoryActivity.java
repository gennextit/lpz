package com.gennext.lpz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gennext.lpz.shop.OrderHistory;
import com.gennext.lpz.util.AppTokens;

/**
 * Created by Abhijit on 15-Dec-16.
 */

public class OrderHistoryActivity extends BaseActivity{
    public static final int CHECKOUT_PROCESS=1,DRAWER_PROCESS=2;
    FragmentManager manager;
    private int actionStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = getSupportFragmentManager();
        Intent intent=getIntent();
        startScreen(intent);
    }

    private void startScreen(Intent intent) {
        if(intent!=null) {
            int data=intent.getIntExtra(AppTokens.ORDER_HISTORY_STATUS,0);
            if(data==CHECKOUT_PROCESS){
                actionStatus=CHECKOUT_PROCESS;
            }else if(data==DRAWER_PROCESS){
                actionStatus=DRAWER_PROCESS;
            }
        }
        OrderHistory orderHistory = new OrderHistory();
        orderHistory.setStatus(CHECKOUT_PROCESS);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, orderHistory, "orderHistory");
        transaction.commit();
    }


    public void onBackButtonAction() {
        if(actionStatus==CHECKOUT_PROCESS){
            Intent intent = new Intent(OrderHistoryActivity.this, ShoppingActivity.class);
            startActivity(intent);
            finish();
        }else if(actionStatus==DRAWER_PROCESS){
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        onBackButtonAction();
        super.onBackPressed();
    }
}
