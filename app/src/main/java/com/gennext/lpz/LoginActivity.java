package com.gennext.lpz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gennext.lpz.login.SignUpMobile;
import com.gennext.lpz.login.SignUpMobileVerify;
import com.gennext.lpz.util.AppTokens;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements SignUpMobile.OnVerifyMobile, SignUpMobileVerify.OnSuccessVerifyedMobile {

    int MOBILE_SCREEN=1,MOBILE_VERIFY_SCREEN=2;
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
        manager=getSupportFragmentManager();
        setScreenDynamically(MOBILE_SCREEN);
        setStatus(getIntent());
    }

    private void setStatus(Intent intent) {
//        if(intent!=null) {
//            int data=intent.getIntExtra(AppTokens.ORDER_HISTORY_STATUS,0);
//            if(data==CHECKOUT_PROCESS){
//                actionStatus=CHECKOUT_PROCESS;
//            }else if(data==DRAWER_PROCESS){
//                actionStatus=DRAWER_PROCESS;
//            }
//        }
    }


    private void setScreenDynamically(int position) {
        FragmentTransaction transaction;
        FragmentManager mannager = getSupportFragmentManager();
        switch (position) {
            case 1:
                SignUpMobile signUpMobile = new SignUpMobile();
                signUpMobile.setCommunicator(LoginActivity.this);
                transaction = mannager.beginTransaction();
                transaction.add(android.R.id.content, signUpMobile, "signUpMobile");
                transaction.commit();
                break;
            case 2:
                SignUpMobileVerify signUpMobileVerify = new SignUpMobileVerify();
                signUpMobileVerify.setCommunicator(LoginActivity.this);
                transaction = mannager.beginTransaction();
                transaction.add(android.R.id.content, signUpMobileVerify, "signUpMobileVerify");
                transaction.addToBackStack("signUpMobileVerify");
                transaction.commit();
                break;
        }
    }





    @Override
    public void onSuccessVerifyedMobile(Boolean status) {
        if (status) {
            AppTokens.setSessionSignup(getApplicationContext(),"success");
            Intent intent=new Intent(LoginActivity.this,ProfileActivity.class);
            intent.putExtra(AppTokens.LOGIN_STATUS, Activity.RESULT_OK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onVerifyMobile(String sportId) {
        setScreenDynamically(MOBILE_VERIFY_SCREEN);
    }

    @Override
    public void onBackPressed() {
        int count=manager.getBackStackEntryCount();
        if ( count > 0) {
           manager.popBackStack();

        } else {
            super.onBackPressed();
            finish();
        }

    }
}

