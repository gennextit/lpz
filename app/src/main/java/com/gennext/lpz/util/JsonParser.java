package com.gennext.lpz.util;

import android.app.Activity;
import android.content.Context;

import com.gennext.lpz.model.DailyNeedsModel;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";

    public JsonModel getDefaultParser(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public DailyNeedsModel getDailyNeedsInfo(Context context,String response) {
        DailyNeedsModel dailyNeedsModel = new DailyNeedsModel();
        dailyNeedsModel.setCatList(new ArrayList<DailyNeedsModel>());
        dailyNeedsModel.setOutput("NA");

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObj = mainArray.getJSONObject(0);
                if(mainObj.optString("status").equals("success")) {
                    JSONObject msgObj = mainObj.optJSONObject("message");
                    JSONObject aboutObj = msgObj.optJSONObject("aboutStore");
                    AppTokens.setAboutUs(context,aboutObj);
                    JSONArray pDetailArray = msgObj.optJSONArray("productDetails");
                    if(pDetailArray!=null)
                    for (int i = 0; i < pDetailArray.length(); i++) {
                        JSONObject spObj = pDetailArray.getJSONObject(i);
                        dailyNeedsModel.setOutput("success");
                        DailyNeedsModel model = new DailyNeedsModel();
                        String catId = spObj.optString("categoryID");
                        model.setCategoryID(catId);
                        model.setCategoryName(spObj.optString("categoryName"));
                        model.setUrl(spObj.optString("url"));
                        model.setDescription(spObj.optString("description"));
                        model.setShowing(true);
                        JSONArray subCatArray = spObj.optJSONArray("subcategory");
                        if (subCatArray != null) {
                            model.setSubCategoryList(new ArrayList<DailyNeedsModel>());
                            model.setOutputSubCat("NA");
//                          model.setSubCategoryJSON(subCatArray.toString());
                            for (int j = 0; j < subCatArray.length(); j++) {
                                JSONObject subCatObj = subCatArray.getJSONObject(j);
                                dailyNeedsModel.setOutputSubCat("success");
                                DailyNeedsModel child = new DailyNeedsModel();
                                String subCatId = subCatObj.optString("subCategoryId");
                                child.setSubCategoryId(subCatId);
                                child.setSubCategoryName(subCatObj.optString("subCategoryName"));

                                JSONArray productArray = subCatObj.optJSONArray("product");
                                if (productArray != null && productArray.length()!=0) {
                                    child.setProductlist(new ArrayList<DailyNeedsModel>());
                                    for (int k = 0; k < productArray.length(); k++) {
                                        JSONObject productObj = productArray.getJSONObject(k);
                                        DailyNeedsModel prodModel = new DailyNeedsModel();
                                        prodModel.setCategoryId(catId);
                                        prodModel.setSubCategoryId(subCatId);
                                        prodModel.setCatPosition(i);
                                        prodModel.setSubCatPosition(j);
                                        prodModel.setProdPosition(k);
                                        prodModel.setProductName(productObj.optString("productName"));
                                        prodModel.setProductID(productObj.optString("productID"));
                                        prodModel.setQuantity("0");
                                        prodModel.setProductRetailPrice(productObj.optString("productRetailPrice"));
                                        prodModel.setProductMRP(productObj.optString("productMRP"));
                                        prodModel.setProductImage(productObj.optString("productImage"));
                                        prodModel.setProductShortDesc(productObj.optString("productShortDesc"));
                                        child.getProductlist().add(prodModel);
                                    }
                                }else{
                                    if(child.getProductlist()==null && child.getSubCategoryName().equalsIgnoreCase("Others")){
                                        model.setShowing(false);
                                    }
                                }


                                //Add Child class object to parent class object
                                model.getSubCategoryList().add(child);
                            }
                        }
                        if(model.getShowing()) {
                            dailyNeedsModel.getCatList().add(model);
                        }

                    }
                }else if(mainObj.optString("status").equals("failure")) {
                    dailyNeedsModel.setOutput("failure");
                    dailyNeedsModel.setOutputMsg(mainObj.optString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return dailyNeedsModel;
    }

    public OrderHistoryModel getOrderHistoryInfo(String response) {
        OrderHistoryModel orderHistoryModel = new OrderHistoryModel();
        orderHistoryModel.setOutput("NA");
        orderHistoryModel.setOutputMsg("No lpz detail available");
        orderHistoryModel.setList(new ArrayList<OrderHistoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONArray messageArr = mainObject.optJSONArray("message");
                    if (messageArr != null) {
                        for (int i = 0; i < messageArr.length(); i++) {
                            JSONObject purchaseItemObj = messageArr.optJSONObject(i);
                            if (purchaseItemObj != null) {
                                orderHistoryModel.setOutput("success");
                                OrderHistoryModel model = new OrderHistoryModel();
                                model.setOrderId(purchaseItemObj.optString("orderId"));
                                model.setOrderTrackId(purchaseItemObj.optString("orderTrackId"));
                                model.setUserId(purchaseItemObj.optString("userId"));
                                model.setOrderTotalAmount(purchaseItemObj.optString("orderTotalAmount"));
                                model.setPaymentMethod(purchaseItemObj.optString("paymentMethod"));
                                model.setOrderShipName(purchaseItemObj.optString("orderShipName"));
                                model.setOrderShipAddress(purchaseItemObj.optString("orderShipAddress"));
                                model.setOrderPhone(purchaseItemObj.optString("orderPhone"));
                                model.setStatus(purchaseItemObj.optString("status"));
                                model.setOrderDate(DateTimeUtility.convertDateMMMYY(purchaseItemObj.optString("orderDate")));
                                model.setOrderTime(DateTimeUtility.convertTime24to12Hours(purchaseItemObj.optString("orderTime")));
//                                model.setDetails(purchaseItemObj.optString("details"));
                                JSONArray detailArr = purchaseItemObj.optJSONArray("details");
                                if (detailArr != null) {
                                    model.setList(new ArrayList<OrderHistoryModel>());
                                    for (int j = 0; j < detailArr.length(); j++) {
                                        JSONObject detailObj = detailArr.optJSONObject(j);
                                        OrderHistoryModel detailModel = new OrderHistoryModel();
                                        detailModel.setProductName(detailObj.optString("productName"));
                                        detailModel.setDetailPrice(detailObj.optString("detailPrice"));
                                        detailModel.setImageUrl(detailObj.optString("productImage"));
                                        detailModel.setDetailQuantity(detailObj.optString("detailQuantity"));
                                        model.getList().add(detailModel);
                                    }
                                }
                                orderHistoryModel.getList().add(model);
                            } else {
                                orderHistoryModel.setOutputMsg("No lpz detail available");
                            }
                        }
                    } else {
                        orderHistoryModel.setOutputMsg("No lpz detail available");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    orderHistoryModel.setOutput("failure");
                    orderHistoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return orderHistoryModel;
    }

    public String[] makeSimpleJsonTask(String response) {
        String[] res;
        res=new String[1];
        res[0]="success";
        return res;
    }

    public JsonModel parseRegJson(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseVerifyOtpJson(Activity activity, String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseUpdateProfileJson(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseCheckoutConfirmResponse(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response ;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }



//    public MyPackageModel parseMyPackageDetail() {
//        MyPackageModel jsonModel = new MyPackageModel();
//        jsonModel.setOutput("success");
//        jsonModel.setOutputMsg("No msg available");
//
//        jsonModel.setNewList(new ArrayList<MyPackageModel>());
//        MyPackageModel model = new MyPackageModel();
//        model.setpName("Silver");
//        model.setpAmount("2500");
//        model.setpValidity("02-12-2017");
//        model.setpCapping("10000 (as per Bronze)");
//
//        jsonModel.getNewList().add(model);
//
//        return jsonModel;
//    }
//
//    public MyPayoutModel parseMyPayoutDetail() {
//        MyPayoutModel jsonModel = new MyPayoutModel();
//        jsonModel.setOutput("success");
//        jsonModel.setOutputMsg("No msg available");
//        MyPayoutModel model;
//        jsonModel.setList(new ArrayList<MyPayoutModel>());
//        model = new MyPayoutModel();
//        model.setPayDate("03/12/16");
//        model.setPayAmount("Rs. 150");
//        model.setPayOutList(setPayoutList());
//        jsonModel.getList().add(model);
//
//        model = new MyPayoutModel();
//        model.setPayDate("26/11/16");
//        model.setPayAmount("Rs. 200");
//        model.setPayOutList(setPayoutList());
//        jsonModel.getList().add(model);
//
//        model = new MyPayoutModel();
//        model.setPayDate("19/11/16");
//        model.setPayAmount("Rs. 250");
//        model.setPayOutList(setPayoutList());
//        jsonModel.getList().add(model);
//
//        model = new MyPayoutModel();
//        model.setPayDate("12/11/16");
//        model.setPayAmount("Rs. 300");
//        model.setPayOutList(setPayoutList());
//        jsonModel.getList().add(model);
//
//        return jsonModel;
//    }
//
//
//    public Model parseLoginData(Activity activity,String response) {
//        Model model = new Model();
//        model.setOutput("NA");
//        model.setOutputMsg("No msg available");
//        if (response.contains("[")) {
//            try {
//                JSONArray mainArray = new JSONArray(response);
//                JSONObject mainObject = mainArray.getJSONObject(0);
//                if (mainObject.getString("status").equals("success")) {
//                    JSONObject msgObj=mainObject.optJSONObject("message");
//                    if(msgObj!=null) {
//                        AppUser.setUserProfile(activity,msgObj.optJSONObject("userProfile"));
//                        AppUser.setBusinessDetails(activity,msgObj.optJSONObject("userProfile"));
//                        AppUser.setPackageDetails(activity,msgObj.optJSONObject("packageDetails"));
//                        AppUser.setDomains(activity,msgObj.optJSONArray("domains"));
//                        AppUser.setKycDetails(activity,msgObj.optJSONArray("kycDetails"));
//                        AppUser.setBankDetails(activity,msgObj.optJSONArray("bankDetails"));
//                    }
//                    model.setOutput("success");
//
//                } else if (mainObject.getString("status").equals("failure")) {
//                    model.setOutput("failure");
//                    model.setOutputMsg(mainObject.getString("message"));
//                }
//            } catch (JSONException e) {
//                ERRORMESSAGE = e.toString();
//                return null;
//            }
//        } else {
//            ERRORMESSAGE = response;
//            return null;
//        }
//        return model;
//    }
}
