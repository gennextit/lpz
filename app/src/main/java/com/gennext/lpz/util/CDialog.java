package com.gennext.lpz.util;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gennext.lpz.R;

/**
 * Created by Abhijit on 19-Dec-16.
 */

public class CDialog {

    private Context context;
    private String message;

    public CDialog(Context context, String message){
        this.context=context;
        this.message=message;
    }

    public AlertDialog show() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_cdialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        TextView tvMessage = (TextView) dialogView.findViewById(R.id.tv_message);
        tvMessage.setText(message);
//        mainLayout.setBackgroundResource(R.drawable.my_progress_one);
//        AnimationDrawable frameAnimation = (AnimationDrawable)mainLayout.getBackground();
//        frameAnimation.start();
        AlertDialog b = dialogBuilder.create();
        b.show();
        return b;
    }

}

