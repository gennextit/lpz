package com.gennext.lpz.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.lpz.LoginActivity;
import com.gennext.lpz.R;
import com.gennext.lpz.common.AboutUs;
import com.gennext.lpz.common.Feedback;
import com.gennext.lpz.dialog.CProgress;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.SideMenu;
import com.gennext.lpz.model.SideMenuAdapter;
import com.gennext.lpz.profile.Profile;
import com.gennext.lpz.shop.OrderHistory;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class CompactFragment extends Fragment {
    public static final int CHECKOUT_PROCESS = 1, DRAWER_PROCESS = 2;
    ProgressDialog progressDialog;
    boolean conn = false;
    public AlertDialog dialog = null;
    public static int OPEN_TO_ALL = 1, INVITE_ONLY = 2, PAID_ENTRY = 3;
    LinearLayout llProgress, llButton;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;
    private AlertDialog alertDialog;

    public CompactFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    public void setActionBarOption(View v, String Title) {
        LinearLayout ActionBack;
        ActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        TextView tvTitle = (TextView) v.findViewById(R.id.actionbar_title);

        tvTitle.setText(Title);
        ActionBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                FragmentManager manager = getFragmentManager();
                manager.popBackStack();
            }
        });

    }

    public void initToolBar(final Activity act, View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(act);
                        getFragmentManager().popBackStack();
                    }
                }

        );
    }


    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    protected void setProgressBar(View v) {
        llProgress = (LinearLayout) v.findViewById(R.id.ll_progress);
    }

    protected void hideProgressBar() {
        Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        Animation animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        if (llProgress != null) {
            llProgress.startAnimation(animFadeOut);
            llProgress.setVisibility(View.GONE);
        }
        if (llButton != null) {
            llButton.startAnimation(animFadeIn);
            llButton.setVisibility(View.VISIBLE);
        }
    }

    protected void hideProgressBar(Activity activity) {
        Animation animFadeIn = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
        Animation animFadeOut = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
        if (llProgress != null) {
            llProgress.startAnimation(animFadeOut);
            llProgress.setVisibility(View.GONE);
        }
        if (llButton != null) {
            llButton.startAnimation(animFadeIn);
            llButton.setVisibility(View.VISIBLE);
        }
    }

    protected void showProgressBar() {
        Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        Animation animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        if (llButton != null) {
            llButton.startAnimation(animFadeOut);
            llButton.setVisibility(View.GONE);
        }
        if (llProgress != null) {
            llProgress.startAnimation(animFadeIn);
            llProgress.setVisibility(View.VISIBLE);
        }
    }

    public void showPopupAlert(String title, String description, int finishType) {
        PopupAlert popupAlert = new PopupAlert();
        popupAlert.setDetail(title, description, finishType);
        addFragment(popupAlert, "popupAlert");
    }


    public CProgress showCProgress(Button button) {
        if (button != null) {
            button.setVisibility(View.GONE);
        }
        return showCProgress();
    }

    public CProgress showCProgress() {
        CProgress cProgress=CProgress.newInstance();
        cProgress.show(getFragmentManager(), "cProgress");
        return cProgress;
    }

    public void removeCProgress(CProgress cProgress,Button button) {
        if (button != null) {
            button.setVisibility(View.VISIBLE);
        }
        removeCProgress(cProgress);
    }

    public void removeCProgress(CProgress cProgress) {
        cProgress.dismiss();
    }


    public static void hideKeybord(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = activity.getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void showProgressDialog(Context context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        CDialog cDialog=new CDialog(context,msg);
        alertDialog=cDialog.show();
    }

    public void hideProgressDialog() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }

    public void setTypsFace(EditText et, EditText et1, EditText et2, EditText et3, EditText et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et1, AutoCompleteTextView et2, AutoCompleteTextView et3, AutoCompleteTextView et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(EditText et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(CheckBox et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(TextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(Button et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }


    public String getSt(int id) {
        return getResources().getString(id);
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = true;
        } else {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = false;
        }

        return conn;
    }

//    public String getMACAddress(Activity activity) {
//        // TODO Auto-generated method stub
//        String address;
//        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
//
//        if (wifiManager.isWifiEnabled()) {
//            // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
//            WifiInfo info = wifiManager.getConnectionInfo();
//            address = info.getMacAddress();
//            // Toast.makeText(getBaseContext(),address,
//            // Toast.LENGTH_SHORT).show();
//
//        } else {
//            // ENABLE THE WIFI FIRST
//            wifiManager.setWifiEnabled(true);
//
//            // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
//            WifiInfo info = wifiManager.getConnectionInfo();
//            address = info.getMacAddress();
//            // Toast.makeText(getBaseContext(),address,
//            // Toast.LENGTH_SHORT).show();
//
//            // DISABLE THE WIFI
//            wifiManager.setWifiEnabled(false);
//
//        }
//        return address;
//    }


    public void showInternetAlertBox(Activity activity) {
        showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg));
    }

    public void showDefaultAlertBox(Activity activity, String title, String Description) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(title);
        tvDescription.setText(Description);

        button1.setText("Setting");
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                EnableMobileIntent();
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public void hideBaseServerErrorAlertBox() {
        if (dialog != null)
            dialog.dismiss();
    }

    public Button showBaseServerErrorAlertBox(String errorDetail) {
        return showBaseAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), errorDetail);
    }

    public Button showBaseServerErrorAlertBox() {
        return showBaseAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), null);
    }

    public Button showBaseAlertBox(String title, String Description, final String errorMessage) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_internet, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        ivAbout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        tvDescription.setText(Description);

        button1.setText("Retry");

        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
        return button1;
    }

    protected void SetDrawer(final Activity act, View v, Toolbar toolbar) {
        // TODO Auto-generated method stub

        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean drawerOpen = dLayout.isDrawerOpen(dList);
                        if (!drawerOpen) {
                            dLayout.openDrawer(dList);
                        } else {
                            dLayout.closeDrawer(dList);
                        }
                    }
                }

        );

        dLayout = (DrawerLayout) v.findViewById(R.id.drawer_layout);
        dList = (ListView) v.findViewById(R.id.left_drawer);
        LayoutInflater inflater = act.getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
        CircleImageView ivProfile = (CircleImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
//        String name= getSt(R.string.app_name);
        String name = AppTokens.getStoreName(act);
        if (name.equals("")) {
            name = getSt(R.string.app_name);
        }
        String image = "";
        tvName.setText(name);

        Glide.with(act)
                .load(image)
                .placeholder(R.drawable.app_logo)
                .error(R.drawable.app_logo)
                .into(ivProfile);

        dList.addHeaderView(listHeaderView);

        SideMenu s1 = new SideMenu("Order History", R.drawable.ic_menu_orders);
        SideMenu s2 = new SideMenu("Profile", R.drawable.ic_menu_profile);
        SideMenu s3 = new SideMenu("Feedback", R.drawable.ic_menu_feedback);
        SideMenu s4 = new SideMenu("About us", R.drawable.ic_menu_about);

        sideMenuList = new ArrayList<>();
        sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(slideMenuAdapter);
        // dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();

                switch (position) {

                    case 1:
                        OrderHistory orderHistory = new OrderHistory();
                        orderHistory.setStatus(DRAWER_PROCESS);
                        addFragment(orderHistory, "orderHistory");
                        break;
                    case 2:
                        if (!AppTokens.getSessionSignup(act).equals("")) {
                            Profile profile = new Profile();
                            profile.setOpenStatus(Profile.DRAWER_SCREEN);
                            addFragment(profile, "profile");
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                        break;
                    case 3:
                        addFragment(new Feedback(), "feedback");
                        break;
                    case 4:
                        addFragment(new AboutUs(), "aboutUs");
                        break;
                }

            }

        });
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    public void addFragWithoutBackStack(Fragment fragment, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, fragment, tag);
        transaction.commit();
    }

    public void removeFragment(String tag) {
        Fragment fragment = getFragmentManager().findFragmentByTag(tag);
        if (fragment != null)
            getFragmentManager().beginTransaction().remove(fragment).commit();
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void setUnSetFragment(FragmentManager manager, int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1) {
        FragmentTransaction ft = manager.beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide other fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }

        ft.commit();
    }
}
