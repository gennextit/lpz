package com.gennext.lpz.util;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {

    public static RequestBody LoginBody(String username, String password) {
        return new FormBody.Builder()
                .addEncoded("userId", username)
                .addEncoded("password", password)
                .build();
    }

    public static RequestBody Mobile(String customerMobile, String mobile) {
        return new FormBody.Builder()
                .addEncoded(customerMobile, mobile)
                .build();
    }

    public static RequestBody VerifyMobile(String customerMobile, String mobile, String otp, String otp1) {
        return new FormBody.Builder()
                .addEncoded(customerMobile, mobile)
                .addEncoded(otp, otp1)
                .build();
    }

    public static RequestBody UpdateProfile(String mobile, String name, String email, String pin, String address) {
        return new FormBody.Builder()
                .addEncoded("customerMobile", mobile)
                .addEncoded("customerName", name)
                .addEncoded("customerEmail", email)
                .addEncoded("gcmId", "")
                .addEncoded("customerPinCode", pin)
                .addEncoded("customerAddress", address)
                .addEncoded("alternatePhone", "")
                .build();
    }

    public static RequestBody Order(String regMobile, String checkOutJson, String payableAmt, String address
            , String mobile, String name,String typeOfOrder) {
        return new FormBody.Builder()
                .addEncoded("customerMobile", regMobile)
                .addEncoded("orderJson", checkOutJson)
                .addEncoded("totalOrderAmount", payableAmt)
                .addEncoded("shippingAddress", address)
                .addEncoded("orderPhone", mobile)
                .addEncoded("customerName", name)
                .addEncoded("typeOfOrder", typeOfOrder)
                .build();
    }

    public static RequestBody OrderConfirm(float rating, String feedback,String customerMobile,String orderId) {
        return new FormBody.Builder()
                .addEncoded("customerMobile", customerMobile)
                .addEncoded("orderId", orderId)
                .addEncoded("rating", String.valueOf(rating))
                .addEncoded("feedback", feedback)
                .build();
    }

    public static RequestBody OrderCancel(String reason,String customerMobile,String orderId) {
        return new FormBody.Builder()
                .addEncoded("customerMobile", customerMobile)
                .addEncoded("orderId", orderId)
                .addEncoded("reason", reason)
                .build();
    }

    public static RequestBody feedBack(String rating, String feedback, String mobile,String email) {
        return new FormBody.Builder()
                .addEncoded("customerMobile", mobile)
                .addEncoded("customerEmail", email)
                .addEncoded("rating", rating)
                .addEncoded("feedback", feedback)
                .build();
    }
}
