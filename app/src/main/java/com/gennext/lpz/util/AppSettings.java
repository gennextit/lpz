package com.gennext.lpz.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */
public class AppSettings {

    public static final String IP_ADDRESS = "http://www.gennextit.com/";


    // Change IP_Address with test ip or production
    public static final String WebServiceAPI = IP_ADDRESS + "grocerystore/Shopping/";

    public static final String LOGIN = WebServiceAPI+"login";
    public static final String REGISTRATION = WebServiceAPI+"registration";//customerMobile
    public static final String VERIFY_OTP = WebServiceAPI+"verifyOtp"; //customerMobile,otp
    public static final String ORDER= WebServiceAPI+"order"; //customerMobile,orderJson,totalOrderAmount,shippingAddress,orderPhone,customerName
    public static final String UPDATE_PROFILE= WebServiceAPI+"updateProfile"; //customerMobile,customerName,customerEmail,gcmId,customerAddress,alternatePhone
    public static final String GET_ORDER_LIST= WebServiceAPI+"getOrderList";  //customerMobile
    public static final String tandc= WebServiceAPI+"tandc";
    public static final String feedback_For_App= WebServiceAPI+"feedbackForApp"; //customerMobile, rating, feedback
    public static final String orderReceived= WebServiceAPI+"orderReceived"; //customerMobile,orderId, rating, feedback
    public static final String orderCancelled= WebServiceAPI+"orderCancelled"; //customerMobile, orderId, reason




}
