package com.gennext.lpz.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gennext.lpz.model.AboutUsModel;

import org.json.JSONObject;

/**
 * Created by Abhijit on 3/5/2016.
 */
public class AppTokens {

    public static final String COMMON = "lpz";

    public static final String APP_USER = "appuser" + COMMON;


    public static final String DOWNLOAD_DIRECTORY_NAME = COMMON;

    // special character to prefix the otp. Make sure this character appears
    // only once in the sms
    public static final String SessionIntro = "SessionIntro" + COMMON;
    public static final String SessionSignup = "SessionSignup" + COMMON;
    public static final String SessionProfile = "SessionProfile" + COMMON;


    public static final String PROFILE_IMAGE_DIRECTORY_NAME = COMMON;
    public static final String FolderDirectory = COMMON;


    public static final String SMS_ORIGIN = "-GENRWA";


    public static final String LOGIN_STATUS = "loginstatus" + COMMON;
    public static final String PROFILE_STATUS = "profilestatus" + COMMON;
    public static final String ORDER_HISTORY_STATUS = "orderhistorystatus" + COMMON;

    private static final String ABOUT_US = "aboutus" + COMMON;
    private static final String ADDRESS = "Address" + COMMON;
    private static final String EMAIL = "email" + COMMON;
    private static final String OPENINGTIME = "openingtime" + COMMON;
    private static final String CLOSINGTIME = "Closingtime" + COMMON;
    private static final String FACEBOOKURL = "facebookurl" + COMMON;
    private static final String GOOGLEPLUSURL = "GooglePlusUrl" + COMMON;
    private static final String WEBSITEURL = "WebsiteUrl" + COMMON;
    private static final String CONTACTNUMBERS = "contactnumbers" + COMMON;
    private static final String STORE_NAME = "sname" + COMMON;

    public static void setAboutUs(Context context, JSONObject aboutUs) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            if (aboutUs!= null) {
                editor.putString(STORE_NAME, aboutUs.optString("name"));
                editor.putString(ABOUT_US, aboutUs.optString("aboutUs"));
                editor.putString(ADDRESS,aboutUs.optString("address"));
                editor.putString(OPENINGTIME, aboutUs.optString("email"));
                editor.putString(EMAIL, aboutUs.optString("openTime"));
                editor.putString(CLOSINGTIME, aboutUs.optString("closeTime"));
                editor.putString(FACEBOOKURL, aboutUs.optString("facebookUrl"));
                editor.putString(GOOGLEPLUSURL, aboutUs.optString("googleplusUrl"));
                editor.putString(CONTACTNUMBERS, aboutUs.optString("contactNumbers"));
                editor.putString(WEBSITEURL, aboutUs.optString("webUrl"));
                editor.apply();
            }
        }
    }

    public static AboutUsModel getAboutUs(Activity context) {
        AboutUsModel aboutUsModel=new AboutUsModel();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        aboutUsModel.setAboutUs(sharedPreferences.getString(ABOUT_US, ""));
        aboutUsModel.setAddress(sharedPreferences.getString(ADDRESS, ""));
        aboutUsModel.setEmail(sharedPreferences.getString(EMAIL, ""));
        aboutUsModel.setOpeningTime(sharedPreferences.getString(OPENINGTIME, ""));
        aboutUsModel.setClosingTime(sharedPreferences.getString(CLOSINGTIME, ""));
        aboutUsModel.setFacebookUrl(sharedPreferences.getString(FACEBOOKURL, ""));
        aboutUsModel.setGooglePlusUrl(sharedPreferences.getString(GOOGLEPLUSURL, ""));
        aboutUsModel.setWebsiteUrl(sharedPreferences.getString(WEBSITEURL, ""));
        aboutUsModel.setStoreName(sharedPreferences.getString(STORE_NAME, ""));
        aboutUsModel.setContactNumbers(sharedPreferences.getString(CONTACTNUMBERS, ""));
        return aboutUsModel;
    }

    public static void setSessionSignup(Context activity, String outputMsg) {
        Utility.SavePref(activity, SessionSignup, outputMsg);
    }

    public static String getSessionSignup(Context activity) {
        return Utility.LoadPref(activity, SessionSignup);
    }

    public static void setSessionProfile(Context activity, String outputMsg) {
        Utility.SavePref(activity, SessionProfile, outputMsg);
    }

    public static String getSessionProfile(Context activity) {
        return Utility.LoadPref(activity, SessionProfile);
    }

    public static String getAboutus(Activity activity) {
        return Utility.LoadPref(activity, ABOUT_US);
    }

    public static String getStoreName(Activity activity) {
        return Utility.LoadPref(activity, STORE_NAME);
    }


    public static String getAddress(Activity activity) {
        return Utility.LoadPref(activity, ADDRESS);
    }

    public static String getEmail(Activity activity) {
        return Utility.LoadPref(activity, EMAIL);
    }

    public static String getOpeningtime(Activity activity) {
        return Utility.LoadPref(activity, OPENINGTIME);
    }

    public static String getClosingtime(Activity activity) {
        return Utility.LoadPref(activity, CLOSINGTIME);
    }

    public static String getFacebookurl(Activity activity) {
        return Utility.LoadPref(activity, FACEBOOKURL);
    }

    public static String getGooglePlusUrl(Activity activity) {
        return Utility.LoadPref(activity, GOOGLEPLUSURL);
    }

    public static String getWebsiteUrl(Activity activity) {
        return Utility.LoadPref(activity, WEBSITEURL);
    }
}
