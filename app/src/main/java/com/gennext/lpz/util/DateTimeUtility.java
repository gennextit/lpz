package com.gennext.lpz.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtility {



	public static String convertTime24to12Hours(String inputTime) {
		DateFormat inputFormat = new SimpleDateFormat("HH:mm");
		DateFormat outputFormat = new SimpleDateFormat("hh:mm a");
		Date date = null;
		String outputDateStr = null;
		try {
			date = inputFormat.parse(inputTime);
			outputDateStr = outputFormat.format(date);
		} catch (ParseException e) {
			L.m(e.toString());
		}
		return outputDateStr;
	}

	public static String convertDateMMM(String inputDate) {
		DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat outputFormat = new SimpleDateFormat("dd-MMM");
		Date date = null;
		String outputDateStr = null;
		try {
			date = inputFormat.parse(inputDate);
			outputDateStr = outputFormat.format(date);
		} catch (ParseException e) {
			L.m(e.toString());
		}
		return outputDateStr;
	}

	public static String convertDateMMMYY(String inputDate) {
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yy");
		Date date = null;
		String outputDateStr = null;
		try {
			date = inputFormat.parse(inputDate);
			outputDateStr = outputFormat.format(date);
		} catch (ParseException e) {
			L.m(e.toString());
		}
		return outputDateStr;
	}

	public static String convertDate(String inputDate) {
		DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = null;
		String outputDateStr = null;
		try {
			date = inputFormat.parse(inputDate);
			outputDateStr = outputFormat.format(date);
		} catch (ParseException e) {
			L.m(e.toString());
		}
		return outputDateStr;
	}

	public static String convertDateMMM2(String inputDate) {
		DateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
		DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = null;
		String outputDateStr = null;
		try {
			date = inputFormat.parse(inputDate);
			outputDateStr = outputFormat.format(date);
		} catch (ParseException e) {
			L.m(e.toString());
		}
		return outputDateStr;
	}

	public static String convertDate(int day, int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		String format = new SimpleDateFormat("d MMMM yyyy, EEEE").format(cal.getTime());

		return format;
	}

	public static String convertDateStamp(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		String format = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());

		return format;
	}
	public static String convertDateTimeStamp(int year, int month, int day,int hour,int min) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE,min); 
		
		String format = new SimpleDateFormat("yyyyMMddHHmm").format(cal.getTime());

		return format;
	}

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public static String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Day = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Year = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}

	public static String convertTime(String time) {
		// String s = "12:18:00";
		String[] res = time.split(":");
		int hr = Integer.parseInt(res[0]);
		String min = res[1];

		if (hr == 12) {
			return (12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
		}

		return (hr % 12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
	}
}
