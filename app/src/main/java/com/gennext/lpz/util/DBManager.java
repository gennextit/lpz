package com.gennext.lpz.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.gennext.lpz.model.CheckoutModel;
import com.gennext.lpz.model.DailyNeedsModel;

import java.util.ArrayList;

public class DBManager {
    // Database Tokens
    SQLiteDatabase db;
    Activity act;
    ArrayList<CheckoutModel> tableList;

    public DBManager(Activity activity) {
        this.act = activity;
        CreateDataBase();
    }

    public void CreateDataBase() {
        db = act.openOrCreateDatabase("lpzDB", Context.MODE_PRIVATE, null);
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS checkout(store_category_id VARCHAR,subcat_id VARCHAR,u_subcat_id VARCHAR,u_subcat_name VARCHAR,u_subcat_price VARCHAR,quantity INTEGER,imageSource VARCHAR);");
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS checkout2(prod_position INTEGER,cat_position INTEGER,subcat_position INTEGER,store_category_id VARCHAR,subcat_id VARCHAR,u_subcat_id VARCHAR,u_subcat_name VARCHAR,u_subcat_price VARCHAR,mrp_price VARCHAR,quantity INTEGER,imageSource VARCHAR);");
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS user(name VARCHAR,mobile VARCHAR,email VARCHAR,state VARCHAR,city VARCHAR,locality VARCHAR,street VARCHAR,houseNo VARCHAR,pinCode VARCHAR);");

        // db.execSQL(
        // "CREATE TABLE IF NOT EXISTS user(name VARCHAR,mobile VARCHAR,email
        // VARCHAR,password VARCHAR,state VARCHAR,city VARCHAR,locality
        // VARCHAR,street VARCHAR,houseNo VARCHAR,pinCode VARCHAR);");
    }

    public void DropTable() {
        db.execSQL("DROP TABLE IF EXISTS checkout");
        db.execSQL("DROP TABLE IF EXISTS checkout2");

    }

//    public void InsertProduct(String store_category_id, String subcat_id, String u_subcat_id, String u_subcat_name,
//                              String u_subcat_price, int quantity, String imageSource) {
//        // Searching product
//        Cursor c = db.rawQuery("SELECT * FROM checkout WHERE store_category_id='" + store_category_id
//                + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'", null);
//        if (c.moveToFirst()) {
//            // Modifying record if found
//            db.execSQL("UPDATE checkout SET quantity='" + quantity + "' WHERE store_category_id='" + store_category_id
//                    + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "' AND imageSource='" + imageSource + "'");
//            L.m("Success Record Updated");
//        } else {
//            L.m("Record not exist than insert query executed");
//            // Inserting record
//
//            db.execSQL(
//                    "INSERT INTO checkout ('store_category_id','subcat_id','u_subcat_id','u_subcat_name','u_subcat_price','quantity','imageSource') VALUES('"
//                            + store_category_id + "','" + subcat_id + "','" + u_subcat_id + "','" + u_subcat_name
//                            + "','" + u_subcat_price + "','" + quantity + "','" + imageSource + "');");
//
//        }
//
//    }

    public void InsertProduct2(int prodPos,int catPos, int subCatPos, String store_category_id, String subcat_id, String u_subcat_id, String u_subcat_name, String u_subcat_price, String mrp,
                               int quantity, String imageSource) {
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM checkout2 WHERE store_category_id='" + store_category_id
                + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'", null);
        if (c.moveToFirst()) {
            // Modifying record if found
            db.execSQL("UPDATE checkout2 SET quantity='" + quantity + "' WHERE store_category_id='" + store_category_id
                    + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "' AND imageSource='" + imageSource + "'");
            L.m("Success Record Updated");
        } else {
            L.m("Record not exist than insert query executed");
            // Inserting record

            db.execSQL(
                    "INSERT INTO checkout2 ('prod_position','cat_position','subcat_position','store_category_id','subcat_id','u_subcat_id','u_subcat_name','u_subcat_price','mrp_price','quantity','imageSource') VALUES(" +
                            " '" + prodPos + "','" + catPos + "','" + subCatPos + "','" + store_category_id + "','" + subcat_id + "','" + u_subcat_id + "','" + u_subcat_name
                            + "','" + u_subcat_price + "','" + mrp + "','" + quantity + "','" + imageSource + "');");

        }

    }

//    public void DeleteProduct(String store_category_id, String subcat_id, String u_subcat_id) {
//        // Searching token number
//        // Searching product
//        Cursor c = db.rawQuery("SELECT * FROM checkout WHERE store_category_id='" + store_category_id
//                + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'", null);
//        if (c.moveToFirst()) {
//            // Deleting record if found
//            db.execSQL("DELETE FROM checkout WHERE store_category_id='" + store_category_id + "' AND subcat_id='"
//                    + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'");
//            L.m("Success " + "Record Deleted");
//        } else {
//            L.m("Error " + "Invalid Product Id");
//        }
//    }

    public void DeleteProduct2(String store_category_id, String subcat_id, String u_subcat_id) {
        // Searching token number
        // Searching product
        Cursor c = db.rawQuery("SELECT * FROM checkout2 WHERE store_category_id='" + store_category_id
                + "' AND subcat_id='" + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'", null);
        if (c.moveToFirst()) {
            // Deleting record if found
            db.execSQL("DELETE FROM checkout2 WHERE store_category_id='" + store_category_id + "' AND subcat_id='"
                    + subcat_id + "' AND u_subcat_id='" + u_subcat_id + "'");
            L.m("Success " + "Record Deleted");
        } else {
            L.m("Error " + "Invalid Product Id");
        }
    }

//    public ArrayList<CheckoutModel> ViewData() {
//        tableList = new ArrayList<CheckoutModel>();
//        // Retrieving all records
//        Cursor c = db.rawQuery("SELECT * FROM checkout", null);
//        // Checking if no records found
//        if (c.getCount() == 0) {
//            L.m("Error No records found");
//            return null;
//        }
//        // Appending records to a string buffer
//        StringBuffer buffer = new StringBuffer();
//        while (c.moveToNext()) {
//            CheckoutModel ob = new CheckoutModel();
//            ob.setCategoryId(c.getString(0));
//            ob.setSubCategoryId(c.getString(1));
//            ob.setProductId(c.getString(2));
//            ob.setProductName(c.getString(3));
//            ob.setProductPrice(c.getString(4));
//            ob.setProductQuantity(c.getString(5));
//            ob.setProductImage(c.getString(6));
//            tableList.add(ob);
////			 buffer.append("tokenId: " + c.getString(0) + "\n");
////			 buffer.append("tokenName: " + c.getString(1) + "\n");
////			 buffer.append("category: " + c.getString(2) + "\n");
////			 buffer.append("remarks: " + c.getString(3) + "\n");
////			 buffer.append("date: " + c.getString(4) + "\n");
////			 buffer.append("time: " + c.getString(5) + "\n\n");
//        }
////		 Displaying all records
////		 L.m("Student Details : "+ buffer.toString());
//        return tableList;
//    }

    public ArrayList<DailyNeedsModel> ViewData2() {
        ArrayList<DailyNeedsModel>checkoutList = new ArrayList<>();
        // Retrieving all records
        Cursor c = db.rawQuery("SELECT * FROM checkout2", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            L.m("Error No records found");
            return null;
        }
        // Appending records to a string buffer
//		StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            DailyNeedsModel ob = new DailyNeedsModel();
            ob.setProdPosition(c.getInt(0));
            ob.setCatPosition(c.getInt(1));
            ob.setSubCatPosition(c.getInt(2));
            ob.setCategoryId(c.getString(3));
            ob.setSubCategoryId(c.getString(4));
            ob.setProductID(c.getString(5));
            ob.setProductName(c.getString(6));
            ob.setProductRetailPrice(c.getString(7));
            ob.setProductMRP(c.getString(8));
            ob.setQuantity(c.getString(9));
            ob.setProductImage(c.getString(10));
            checkoutList.add(ob);
        }
        return checkoutList;
    }

    public void UpdateUserData(String mobile, String name, String email, String state, String city, String locality,
                               String street, String houseNo, String pinCode) {
        // Searching token number
        Cursor c = db.rawQuery("SELECT * FROM user WHERE mobile='" + mobile + "'", null);
        if (c.moveToFirst()) {
            // Modifying record if found
            db.execSQL("UPDATE user SET name='" + name + "',mobile='" + mobile + "',state='" + state + "',city='" + city
                    + "',locality='" + locality + "',street='" + street + "',houseNo='" + houseNo + "',pinCode='" + pinCode
                    + "' WHERE mobile='" + mobile + "'");
            L.m("Success Record Updated");
        } else {
            db.execSQL("INSERT INTO user ('name','mobile','email','state','city','locality','street','houseNo','pinCode') VALUES('" + name + "','" + mobile + "','" + email + "','" + state + "','" + city + "','" + locality + "','" + street + "','" + houseNo + "','" + pinCode + "');");
            L.m("Success Record Inserted");
        }

    }


    public Cursor ViewUserData(String mobile) {
        // Inserting record
        // Retrieving all records
        Cursor c = db.rawQuery("SELECT * FROM user WHERE mobile='" + mobile + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            L.m("Error No records found");
            return null;
        }
        return c;
    }

    public void CloseDB() {
        db.close();
    }
}
