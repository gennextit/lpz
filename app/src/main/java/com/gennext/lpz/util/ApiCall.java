package com.gennext.lpz.util;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class ApiCall {
    //GET network request
    public static String GET(String url){
        String res = null;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            res=response.body().string();
        } catch (IOException e) {
            return e.toString();
        }
        return res;
    }

    //POST network request
    public static String POST( String url, RequestBody body){
        String res = null;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            res=response.body().string();
        } catch (IOException e) {
            return e.toString();
        }
        return res;
    }
}