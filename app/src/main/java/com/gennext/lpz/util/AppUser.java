package com.gennext.lpz.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String COMMON = "lpz";


    public static final String NAME = "name" + COMMON;
    public static final String MOBILE = "mobile" + COMMON;
    public static final String EMAIL_ADDRESS = "emailAddress" + COMMON;
    public static final String PIN_CODE = "pincode" + COMMON;
    public static final String ADDRESS = "address" + COMMON;


    public static void setUserProfile(Context context, String name,String email,String pin,String address) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            if (name != null)
                editor.putString(NAME, name);
            if (email!= null)
                editor.putString(EMAIL_ADDRESS, email);
            if (pin != null)
                editor.putString(PIN_CODE,pin);
            if (address != null)
                editor.putString(ADDRESS, address);

            editor.apply();
        }
    }



    public static String LoadPref(Context context, String key) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static String getMOBILE(Context context) {
        return LoadPref(context, MOBILE);
    }
    public static String getName(Activity act) {
        return LoadPref(act, NAME);
    }
    public static String getEmailAddress(Context context) {
        return LoadPref(context, EMAIL_ADDRESS);
    }
    public static String getPinCode(Activity act) {
        return LoadPref(act, PIN_CODE);
    }
    public static String getAddress(Context context) {
        return LoadPref(context, ADDRESS);
    }


    public static void setMobile(Activity context, String mobile) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null && mobile != null) {
            editor.putString(MOBILE,mobile);
            editor.apply();
        }
    }


    public static String[] getProfileData(FragmentActivity context) {
        String[] data = new String[4];
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        data[0] = sharedPreferences.getString(NAME, "");
        data[1] = sharedPreferences.getString(EMAIL_ADDRESS, "");
        data[2] = sharedPreferences.getString(PIN_CODE, "");
        data[3] = sharedPreferences.getString(ADDRESS, "");
        return data;
    }
}
