package com.gennext.lpz.login;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gennext.lpz.R;
import com.gennext.lpz.common.AppWebView;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

public class SignUpMobile extends CompactFragment {

    private EditText etMobile;
    private Button btnSignIn;
    private ProgressBar progressBar;
    private OnVerifyMobile comm;
    HttpTask httpTask;
    FragmentManager mannager;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (httpTask != null) {
            httpTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (httpTask != null) {
            httpTask.onDetach();
        }
    }

    public void setCommunicator(OnVerifyMobile onVerifyMobile) {
        this.comm = onVerifyMobile;
    }

    public interface OnVerifyMobile {
        public void onVerifyMobile(String sportId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.fragment_mobile_singup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        etMobile = (EditText) view.findViewById(R.id.et_mobile_signup);
        btnSignIn = (Button) view.findViewById(R.id.btn_mobile_signin);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        LinearLayout lltandc = (LinearLayout) view.findViewById(R.id.lltandc);
        lltandc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AppWebView appWebView=new AppWebView();
                appWebView.setUrl(getResources().getString(R.string.t_and_c), AppSettings.tandc, AppWebView.URL);
                addFragment(appWebView,android.R.id.content, "appWebView");
            }
        });

        mannager = getFragmentManager();

        btnSignIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                if(isOnline()) {
                    executeTask();
                }else{
                    showInternetAlertBox(getActivity());
                }
            }
        });
    }

    private void executeTask() {
        httpTask = new HttpTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString());
        httpTask.execute(AppSettings.REGISTRATION);
    }

    private class HttpTask extends AsyncTask<String, Void, JsonModel> {
        Button btn;
        ProgressBar pBar;
        Activity activity;
        String name, mobile;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public HttpTask(Activity activity, Button btn, ProgressBar pBar, String mobile) {
            this.activity = activity;
            this.btn = btn;
            this.pBar = pBar;
            this.mobile = mobile;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
            btn.setVisibility(View.GONE);
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response = "error";
            response = ApiCall.POST(urls[0], RequestBuilder.Mobile("customerMobile",mobile));

            JsonParser jsonParser=new JsonParser();
            JsonModel jsonModel=jsonParser.parseRegJson(response);
            return jsonModel;
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            if (activity != null) {
                pBar.setVisibility(View.GONE);
                btn.setVisibility(View.VISIBLE);
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        AppUser.setMobile(activity,mobile);
                        comm.onVerifyMobile(result.getOutput());
                    } else {
                        Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialog != null) dialog.dismiss();
                            executeTask();
                        }
                    });
                }
            }
        }
    }



}
