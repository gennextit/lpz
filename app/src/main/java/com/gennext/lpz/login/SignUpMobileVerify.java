package com.gennext.lpz.login;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.lpz.R;
import com.gennext.lpz.common.AppWebView;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

import java.util.concurrent.TimeUnit;

public class SignUpMobileVerify extends CompactFragment {
    public static final int VERIFY_TASK = 1, GET_SOCIERY_TASK = 2, RESEND_OTP = 3;

    private ProgressBar progressBar, progressBarResentOtp;
    private OnSuccessVerifyedMobile comm;

    HttpTask httpTask;
    private EditText etMobile;
    private Button btnSignIn, btnResendOtp;
    private long timeInMSec = 60000;
    private TextView tvTimerCount;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (httpTask != null) {
            httpTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (httpTask != null) {
            httpTask.onDetach();
        }
    }

    public void setCommunicator(OnSuccessVerifyedMobile onSuccessVerifyedMobile) {
        this.comm = onSuccessVerifyedMobile;
    }

    public interface OnSuccessVerifyedMobile {
        void onSuccessVerifyedMobile(Boolean status);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.fragment_mobile_verify, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        etMobile = (EditText) view.findViewById(R.id.et_mobile_signup);
        tvTimerCount = (TextView) view.findViewById(R.id.tv_timer_count);
        btnSignIn = (Button) view.findViewById(R.id.btn_mobile_signin);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        btnResendOtp = (Button) view.findViewById(R.id.btn_mobile_retry);
        progressBarResentOtp = (ProgressBar) view.findViewById(R.id.progressBar2);
        LinearLayout lltandc = (LinearLayout) view.findViewById(R.id.lltandc);
        lltandc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppWebView appWebView = new AppWebView();
                appWebView.setUrl(getResources().getString(R.string.t_and_c), AppSettings.tandc, AppWebView.URL);
                addFragment(appWebView, android.R.id.content, "appWebView");
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                executeTask(VERIFY_TASK);
            }
        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                executeTask(RESEND_OTP);
            }
        });

        startCountdownTimer();
    }

    private void startCountdownTimer() {
        CounterClass timer = new CounterClass(timeInMSec, 1000);
        timer.start();
    }

    private void executeTask(int task) {
        if (task == GET_SOCIERY_TASK) {
//            httpTask = new HttpTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString(), task);
//            httpTask.execute(AppSettings.getSocietyList);
        } else if (task == VERIFY_TASK) {
            httpTask = new HttpTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString(), task);
            httpTask.execute(AppSettings.VERIFY_OTP);
        } else if (task == RESEND_OTP) {
            httpTask = new HttpTask(getActivity(), btnResendOtp, progressBarResentOtp, etMobile.getText().toString(), task);
            httpTask.execute(AppSettings.REGISTRATION);
        }
    }


    private class HttpTask extends AsyncTask<String, Void, JsonModel> {
        Button btn;
        ProgressBar pBar;
        Activity activity;
        String name, otp;
        int task;

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        public HttpTask(Activity activity, Button btn, ProgressBar pBar, String otp, int task) {
            this.activity = activity;
            this.btn = btn;
            this.pBar = pBar;
            this.otp = otp;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
            btn.setVisibility(View.GONE);
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response = "error";
            String mobile = "";
            JsonParser jsonParser;
            if (activity != null)
                mobile = AppUser.getMOBILE(activity);

            if (task == VERIFY_TASK) {
                response = ApiCall.POST(urls[0], RequestBuilder.VerifyMobile("customerMobile", mobile, "otp", otp));
                jsonParser = new JsonParser();
                return jsonParser.parseVerifyOtpJson(activity, response);
            } else if (task == RESEND_OTP) {
                response = ApiCall.POST(urls[0], RequestBuilder.Mobile("customerMobile", mobile));
                jsonParser = new JsonParser();
                return jsonParser.parseRegJson(response);
            } else if (task == GET_SOCIERY_TASK) {
//                jsonParser = new JsonParser();
//                return jsonParser.parseSocietyListJson(activity, response);
            }
            return null;
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            if (activity != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == VERIFY_TASK) {
                            comm.onSuccessVerifyedMobile(true);
                        } else if (task == GET_SOCIERY_TASK) {
                            pBar.setVisibility(View.GONE);
                            btn.setVisibility(View.VISIBLE);
                            comm.onSuccessVerifyedMobile(true);
                        } else if (task == RESEND_OTP) {
                            pBar.setVisibility(View.GONE);
                            btn.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        pBar.setVisibility(View.GONE);
                        btn.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    pBar.setVisibility(View.GONE);
                    btn.setVisibility(View.VISIBLE);
                    if (task == VERIFY_TASK) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (dialog != null) dialog.dismiss();
                                executeTask(task);
                            }
                        });
                    }
                    if (task == GET_SOCIERY_TASK) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (dialog != null) dialog.dismiss();
                                executeTask(task);
                            }
                        });
                    } else if (task == VERIFY_TASK) {
                        Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (dialog != null) dialog.dismiss();
                                executeTask(task);
                            }
                        });
                    }
                }
            }
        }
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTimerCount.setVisibility(View.GONE);
            btnResendOtp.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

            tvTimerCount.setText("Wait " + hms.substring(3, 5) + " sec");

        }

    }

//    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            int resultCode = intent.getIntExtra("CSResultCode", getActivity().RESULT_CANCELED);
//            if (resultCode == getActivity().RESULT_OK) {
//                String resultOtp = intent.getStringExtra("otp");
//                String status = intent.getStringExtra("status");
//
//                if (status.equalsIgnoreCase("otp")) {
//                    etMobile.setText(resultOtp);
//                    hideKeybord(getActivity());
//                    executeTask(VERIFY_TASK);
//                }
//            }
//        }
//    };
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        IntentFilter filter = new IntentFilter(SmsReceiver.ACTION);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        // Unregister the listener when the application is paused
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
//        // or `unregisterReceiver(testReceiver)` for a normal broadcast
//    }
}
