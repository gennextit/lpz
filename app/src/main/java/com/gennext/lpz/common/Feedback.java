package com.gennext.lpz.common;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gennext.lpz.R;
import com.gennext.lpz.dialog.PopupAlert;
import com.gennext.lpz.model.JsonModel;
import com.gennext.lpz.util.AnimationClass;
import com.gennext.lpz.util.ApiCall;
import com.gennext.lpz.util.AppSettings;
import com.gennext.lpz.util.AppUser;
import com.gennext.lpz.util.CompactFragment;
import com.gennext.lpz.util.JsonParser;
import com.gennext.lpz.util.RequestBuilder;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class Feedback extends CompactFragment {
    private Button btnOK;
    private FragmentManager manager;
    AssignTask assignTask;
    private EditText etFeedback;
    private RatingBar rbRating;
    private RelativeLayout RLView;
    private AnimationClass anim;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_feedback, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
//        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        RLView = (RelativeLayout) v.findViewById(R.id.llappFeedback);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
        rbRating = (RatingBar) v.findViewById(R.id.ratingBar);

        anim=new AnimationClass();
        anim.setPopupAnimation(RLView);
//        tvTitle.setText(title);



        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                sendFeedback(String.valueOf(rbRating.getRating()), etFeedback.getText().toString(),
                        AppUser.getMOBILE(getActivity()),AppUser.getEmailAddress(getActivity()));
            }
        });
    }

    private void sendFeedback(String rating, String feedback, String mobile,String email) {
        assignTask = new AssignTask(getActivity(),rating,feedback,mobile,email);
        assignTask.execute(AppSettings.feedback_For_App);
    }

    private class AssignTask extends AsyncTask<String, Void, JsonModel> {
        Activity activity;
        private String rating,feedback,mobile,email;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String rating, String feedback, String mobile,String email) {
            this.rating=rating;
            this.feedback=feedback;
            this.mobile=mobile;
            this.activity = activity;
            this.email=email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity,"Processing please wait...");
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response = ApiCall.POST(urls[0], RequestBuilder.feedBack(rating,feedback,mobile,email));
            JsonParser jsonParser = new JsonParser();
            if(activity!=null) {
                return jsonParser.getDefaultParser(response);
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Toast.makeText(activity,result.getOutputMsg(),Toast.LENGTH_LONG).show();
                        getFragmentManager().popBackStack();
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            sendFeedback(String.valueOf(rating), feedback, mobile,email);
                        }
                    });
                }
            }
        }
    }
}