package com.gennext.lpz.common;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.model.AboutUsModel;
import com.gennext.lpz.util.AppTokens;
import com.gennext.lpz.util.CompactFragment;


/**
 * Created by Abhijit on 05-Aug-16.
 */
public class AboutUs extends CompactFragment implements View.OnClickListener {
    ImageView ivProfile;
    TextView tvAbout;
    Activity activity;
    ImageView ivFacebook, ivGooglePlus, ivWebsite;
    TextView tvWebsite, tvAddress, tvEmail, tvOpeningTime, tvClosingTime;
    private static final int FACEBOOK = 1, GOOGLE = 2, WEBSITE = 3;
    private AboutUsModel aboutUsModel;
    private LinearLayout llWebControl;
    private TextView tvContactNo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_aboutus, container, false);
        initToolBar(getActivity(),v, getSt(R.string.app_name));
        activity = getActivity(); 
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        ivProfile = (ImageView) v.findViewById(R.id.iv_about_us_profile);
        tvAbout = (TextView) v.findViewById(R.id.tv_about_us_detail); 

        ivFacebook = (ImageView) v.findViewById(R.id.iv_about_facebook); 
        ivGooglePlus = (ImageView) v.findViewById(R.id.iv_about_google_plus);
        ivWebsite = (ImageView) v.findViewById(R.id.iv_about_website);
        llWebControl = (LinearLayout) v.findViewById(R.id.part2);
        ivFacebook.setOnClickListener(this);
        ivGooglePlus.setOnClickListener(this);
        ivWebsite.setOnClickListener(this);


        tvWebsite = (TextView) v.findViewById(R.id.tv_about_website);
        tvAddress = (TextView) v.findViewById(R.id.tv_about_address);
        tvEmail = (TextView) v.findViewById(R.id.tv_about_email);
        tvContactNo = (TextView) v.findViewById(R.id.tv_about_contact);
        tvOpeningTime = (TextView) v.findViewById(R.id.tv_about_opening_time);
        tvClosingTime = (TextView) v.findViewById(R.id.tv_about_closing_time);

        aboutUsModel=AppTokens.getAboutUs(getActivity());

        if(aboutUsModel.getWebsiteUrl().equals("")&& aboutUsModel.getFacebookUrl().equals("")&&
                aboutUsModel.getGooglePlusUrl().equals("")){
            llWebControl.setVisibility(View.GONE);
        }

        if(aboutUsModel.getWebsiteUrl().equals("")){
            ivWebsite.setVisibility(View.GONE);
        }
        if(aboutUsModel.getFacebookUrl().equals("")){
            ivFacebook.setVisibility(View.GONE);
        }
        if(aboutUsModel.getGooglePlusUrl().equals("")){
            ivGooglePlus.setVisibility(View.GONE);
        }

        tvAbout.setText(aboutUsModel.getAboutUs());
        tvAddress.setText(aboutUsModel.getAddress());
        tvEmail.setText(aboutUsModel.getEmail());
        tvContactNo.setText(aboutUsModel.getContactNumbers());
        tvOpeningTime.setText(aboutUsModel.getOpeningTime());
        tvClosingTime.setText(aboutUsModel.getClosingTime());

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_about_facebook:
                showDefaultAlertBox("facebook", aboutUsModel.getFacebookUrl(), FACEBOOK);
                break;
            case R.id.iv_about_google_plus:
                showDefaultAlertBox("Google Plus", aboutUsModel.getGooglePlusUrl(), GOOGLE);
                break;
            case R.id.iv_about_website:
                showDefaultAlertBox("Website", aboutUsModel.getWebsiteUrl(), WEBSITE);
                break;
        }
    }

    public void showDefaultAlertBox(String title, final String url, final int switchStatus) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText("Open As");
        tvDescription.setText("Open " + title + " page in internal or external app?");

        button1.setText("Internal");
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                openInInternal(switchStatus, url);
            }
        });
        button2.setText("External");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                openInExternal(getActivity(), switchStatus, url);
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
    }


    public void openInExternal(Context context, int switchStatus, String url) {
        switch (switchStatus) {
            case FACEBOOK:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;
            case GOOGLE:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;
            case WEBSITE:
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                break;

        }
    }

    public void openInInternal(int switchStatus, String url) {
        AppWebView appWebView = new AppWebView();
        FragmentTransaction transaction;
        switch (switchStatus) {
            case FACEBOOK:
                appWebView.setUrl("FACEBOOK", url, AppWebView.URL);
                transaction = getFragmentManager().beginTransaction();
                transaction.add(android.R.id.content, appWebView, "appWebView");
                transaction.addToBackStack("appWebView");
                transaction.commit();
                break;
           case GOOGLE:
                appWebView.setUrl("GOOGLE", url, AppWebView.URL);
                transaction = getFragmentManager().beginTransaction();
                transaction.add(android.R.id.content, appWebView, "appWebView");
                transaction.addToBackStack("appWebView");
                transaction.commit();
                break;
            case WEBSITE:
                appWebView.setUrl("WEBSITE", url, AppWebView.URL);
                transaction = getFragmentManager().beginTransaction();
                transaction.add(android.R.id.content, appWebView, "appWebView");
                transaction.addToBackStack("appWebView");
                transaction.commit();
                break;
        }
    }
}

