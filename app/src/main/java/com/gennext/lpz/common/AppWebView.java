package com.gennext.lpz.common;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.util.CompactFragment;


public class AppWebView extends CompactFragment {
	String BrowserSetting = "";
	Activity context;
	WebView webView;
	ProgressBar progressBar;
	FragmentManager mannager;
	String webUrl;
	int URLTYPE=0;
	String browserName;
	public static int URL=0, PDF=1;
	
	
	public void setUrl(String browserName, String url, int urlType) {
		this.browserName=browserName;
		this.webUrl=url;
		this.URLTYPE=urlType;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.app_web_view, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		initUi(v);
		return v;
	}


	private void initUi(View v) {
		// TODO Auto-generated method stub
		webView = (WebView) v.findViewById(R.id.webView_browser);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		webView.setInitialScale(0);
		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				progressBar.setProgress(progress);
				if (progress == 100) {
					progressBar.setVisibility(View.GONE);
				} else {
					progressBar.setVisibility(View.VISIBLE);
				}
			}
		});
		// Javascript inabled on webview

		if (isOnline() == true) {
			
			if(URLTYPE==URL){
				startWebView(webUrl);
			}else{
			    String Doc="<iframe src='http://docs.google.com/gview?embedded=true&url="+webUrl+"' width='100%' height='100%' style='border: none;'></iframe>";
			    startWebView(Doc);
			}
		} else {
			AlertInternet();
		}

	}

	public class myWebclient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

	}

	private void startWebView(String url) {
		webView.setWebViewClient(new WebViewClient() {
			
			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if(URLTYPE==URL){
					view.loadUrl(url);
		        }else{
					view.loadData(url, "text/html", "UTF-8");
				}
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {

			}

			public void onPageFinished(WebView view, String url) {

			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
//				Toast.makeText(getActivity(), "Your Internet Connection May not be active Or " + errorCode , Toast.LENGTH_LONG).show();
//				hideProgressDialog();
				Button retry=showBaseServerErrorAlertBox(description+"\n"+errorCode);
				retry.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						hideBaseServerErrorAlertBox();
						startWebView(webUrl);
					}
				});
			}

		});
		
		

		// Javascript inabled on webview
		webView.getSettings().setJavaScriptEnabled(true);

		// Other webview options 
		webView.getSettings().setAllowFileAccess(true);
	    webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		//webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		//webView.setScrollbarFadingEnabled(false); 
		if(URLTYPE==URL){
			webView.loadUrl(url);
        }else{
    		webView.loadData( url , "text/html", "UTF-8");
		}
	}

	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		actionbarTitle.setText(browserName);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (webView.canGoBack()) {
					webView.goBack();
				} else {
					// Let the system handle the back button
					mannager.popBackStack();
				}
			}
		});
	}
	
	// Detect when the back button is pressed
	public void onBackPressed() {

		if (webView.canGoBack()) {
			webView.goBack();
		} else {
			// Let the system handle the back button
			mannager.popBackStack();
		}
	}

	public void AlertInternet() {

		new AlertDialog.Builder(getActivity()).setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Not Connected!").setMessage("Please Check Your Internet Connection.")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}

				}).show();
		// Toast.makeText(getApplicationContext(),"No Internet Connection
		// found...",Toast.LENGTH_LONG).show();

	}

}

