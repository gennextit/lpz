package com.gennext.lpz.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gennext.lpz.R;


public class OrderCancelDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private OrderCancelDialog fragment;
    private EditText etReason;
    private String orderTrackId;

    public interface OrderCancelListener {
          void onOrderCancelClick(DialogFragment dialog,String reason,String orderTrackId);
    }

    private OrderCancelListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static OrderCancelDialog newInstance(String orderTrackId, OrderCancelListener listener) {
        OrderCancelDialog fragment = new OrderCancelDialog();
        fragment.orderTrackId=orderTrackId;
        fragment.mListener = listener;
        fragment.fragment=fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_order_cancel, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        etReason = (EditText) v.findViewById(R.id.et_alert_reason);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onOrderCancelClick(OrderCancelDialog.this,etReason.getText().toString(),orderTrackId);
                }
                if(fragment!=null){
                    fragment.dismiss();
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
