package com.gennext.lpz.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.gennext.lpz.R;


public class OrderConfirmDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private OrderConfirmDialog fragment;
    private RatingBar rbRating;
    private EditText erFeedback;
    private String orderTrackId;

    public interface OrderConfirmDialogListener {
        void onOrderConfirmClick(DialogFragment dialog,float rating,String feedback,String orderTrackId);
    }

    private OrderConfirmDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static OrderConfirmDialog newInstance(String orderTrackId, OrderConfirmDialogListener listener) {
        OrderConfirmDialog fragment = new OrderConfirmDialog();
        fragment.orderTrackId=orderTrackId;
        fragment.mListener = listener;
        fragment.fragment=fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_order_confirm, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        rbRating = (RatingBar) v.findViewById(R.id.ratingBar);
        erFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onOrderConfirmClick(OrderConfirmDialog.this,rbRating.getRating(),erFeedback.getText().toString(),orderTrackId);
                }
                if(fragment!=null){
                    fragment.dismiss();
                }
            }
        });


        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
