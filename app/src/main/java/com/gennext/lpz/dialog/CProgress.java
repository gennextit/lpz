package com.gennext.lpz.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.gennext.lpz.R;


public class CProgress extends DialogFragment {
    private AlertDialog dialog = null;
    public CProgress fragment;


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }



    public static CProgress newInstance() {
        CProgress fragment = new CProgress();
        fragment.fragment=fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cprogress, null);
        dialogBuilder.setView(v);

        dialog = dialogBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        return dialog;
    }

    private void dismissDialog() {
        dialog.dismiss();
        if(fragment!=null){
            fragment.dismiss();
        }
    }
}
