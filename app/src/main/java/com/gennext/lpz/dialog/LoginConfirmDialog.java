package com.gennext.lpz.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gennext.lpz.R;
import com.gennext.lpz.util.AppTokens;


public class LoginConfirmDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private LoginConfirmDialog fragment;

    public interface LoginConfirmDialogListener {
        void onLoginConfirmClick(DialogFragment dialog);
    }

    private LoginConfirmDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static LoginConfirmDialog newInstance(LoginConfirmDialogListener listener) {
        LoginConfirmDialog fragment = new LoginConfirmDialog();
        fragment.mListener = listener;
        fragment.fragment=fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_login_confirm, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        if(AppTokens.getSessionSignup(getActivity()).equals("")) {
            tvTitle.setText("Login");
            button1.setText("Login");
            tvDescription.setText("Please login before checkout process");
        }else if(AppTokens.getSessionProfile(getActivity()).equals("")) {
            button1.setText("OK");
            tvTitle.setText("Profile");
            tvDescription.setText("Please update profile before checkout process");
        }

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onLoginConfirmClick(LoginConfirmDialog.this);
                }
                if(fragment!=null){
                    fragment.dismiss();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });


        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
