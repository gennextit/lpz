package com.gennext.lpz;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.gennext.lpz.model.SideMenu;
import com.gennext.lpz.model.SideMenuAdapter;
import com.gennext.lpz.util.AppUser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BaseActivity extends AppCompatActivity {
    public static final int CHECKOUT_PROCESS=1,DRAWER_PROCESS=2;
    public static final int OPEN=1,CLOSE=2;
    boolean conn = false;
    Builder alertDialog;
    ProgressDialog progressDialog;
    AlertDialog dialog = null;
    ImageView ActionBack;
    TextView ActionBarHeading;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;

    static int COACH = 1, VENUE = 2;
    protected int FINISH_ACTIVITY = 1, FINISH_FRAGMENT = 2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    public void initToolBar(final Activity activity, String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.finish();
                    }
                }

        );
    }

    public Toolbar setToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_menu_white);
        return toolbar;
    }

    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    public SpannableString setBoldFont(int colorId, int typeFaceStyle, String title) {
        SpannableString s = new SpannableString(title);
        Typeface externalFont = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/segoeui.ttf");
        s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }



    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideActionBar() {
        getSupportActionBar().hide();
    }

    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    public SpannableStringBuilder subScr(int text) {

        SpannableStringBuilder cs = new SpannableStringBuilder(String.valueOf(text));
        cs.setSpan(new SubscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        cs.setSpan(new RelativeSizeSpan(0.75f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return cs;
    }

    // ProgressDialog progressDialog; I have declared earlier.
    public void showPDialog(String msg) {
        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }


    public String LoadPref(String key) {
        return LoadPref(BaseActivity.this, key);
    }

    public String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {
        SavePref(BaseActivity.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            conn = true;
        } else {
            conn = false;
        }

        return conn;
    }



    public String getSt(int id) {

        return getResources().getString(id);
    }

    private void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }


    public void showToast(String txt) {
        // Inflate the Layout
        Toast toast = Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
        toast.show();
    }




    protected void SetDrawer(final Activity act, Toolbar toolbar) {
        // TODO Auto-generated method stub

        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean drawerOpen = dLayout.isDrawerOpen(dList);
                        if (!drawerOpen) {
                            dLayout.openDrawer(dList);
                        } else {
                            dLayout.closeDrawer(dList);
                        }
                    }
                }

        );

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
        CircleImageView ivProfile = (CircleImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
        String name= AppUser.getName(act);
        String image= "";
        tvName.setText(name);

        Glide.with(act)
                .load(image)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(ivProfile);

        dList.addHeaderView(listHeaderView);

        SideMenu s1 = new SideMenu("Book A Slot for Consultancy", R.mipmap.ic_launcher);
        SideMenu s2 = new SideMenu("Cancel A Slot", R.mipmap.ic_launcher);
        SideMenu s3 = new SideMenu("My Team", R.mipmap.ic_launcher);
        SideMenu s4 = new SideMenu("My Sales", R.mipmap.ic_launcher);
        SideMenu s5 = new SideMenu("My Payout", R.mipmap.ic_launcher);
        SideMenu s6 = new SideMenu("Direct Referral Income", R.mipmap.ic_launcher);
        SideMenu s7 = new SideMenu("Helpdesk", R.mipmap.ic_launcher);
        SideMenu s8 = new SideMenu("My Profile", R.mipmap.ic_launcher);
        SideMenu s9 = new SideMenu("My Package", R.mipmap.ic_launcher);

        sideMenuList = new ArrayList<>();
        sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        sideMenuList.add(s5);
        sideMenuList.add(s6);
        sideMenuList.add(s7);
        sideMenuList.add(s8);
        sideMenuList.add(s9);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(slideMenuAdapter);
        // dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();

                switch (position) {

                    case 1:
//                        addFragment(new BookASlot(), "bookASlot");
                        break;
                    case 2:
//                        addFragment(new CancelASlot(), "cancelASlot");
                        break;
                    case 3:
//                        addFragment(new MyTeam(), "myTeam");
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        break;
                    case 7:
//                        addFragment(new Helpdesk(), "helpdesk");
                        break;
                    case 8:
                        break;
                    case 9:
                        break;

                }

            }

        });
    }

    public void hideBaseServerErrorAlertBox() {
        if (dialog != null)
            dialog.dismiss();
    }

    public Button showBaseServerErrorAlertBox(Activity activity,String errorDetail) {
        return showBaseAlertBox(activity,getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), errorDetail);
    }

    public Button showBaseServerErrorAlertBox(Activity activity) {
        return showBaseAlertBox(activity,getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), null);
    }

    public Button showBaseAlertBox(Activity activity,String title, String Description, final String errorMessage) {

        final Builder dialogBuilder = new Builder(activity);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_internet, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        ivAbout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        tvDescription.setText(Description);

        button1.setText("Retry");

        button2.setText("Cancel");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
        return button1;
    }

    public void showProgressDialog(Activity context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack("tag");
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment,android.R.id.content, tag);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }
}